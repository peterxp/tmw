<?php


### Frontend ###
Route::get('/', function()
{
    // return '<br/><center>Under Construction! Please Contact Admin</center>';
    return Redirect::to('/en/home');
});

$languages = array('th','en');
$locale = Request::segment(1);

if(in_array($locale, $languages)){
    \App::setLocale($locale);
}else{
    $locale = null;
}

Route::group(array('prefix' => $locale), function()
{

    Route::resource('home', 'frontend\HomesController@index');
    Route::resource('companyprofile', 'frontend\HomesController@companyprofile');
    //Route::get('product_category', 'frontend\HomesController@productCategory');
    //Route::get('product_category/{category_id}/{category_name}', 'frontend\HomesController@productByCategoryId');
    //Route::get('productByCatAjax/{category_id}', 'frontend\HomesController@productByCatAjax');
    
    Route::resource('portfolio', 'frontend\HomesController@portfolio');
    Route::get('portfolio/{category_id}/{category_name}/{any?}', 'frontend\HomesController@projectByCategoryId');
    Route::get('project_detail/{project_id}', 'frontend\HomesController@projectDetail');

    Route::resource('activities', 'frontend\HomesController@activity');
    Route::get('activity_detail/{activity_id}', 'frontend\HomesController@activtyDetail');
    
    Route::resource('our_customers', 'frontend\HomesController@ourCustomers');
    Route::resource('aboutus', 'frontend\HomesController@aboutus');
    Route::resource('job-vacancy', 'frontend\HomesController@jobVacancy');
    Route::resource('contactus', 'frontend\HomesController@contactus');
    Route::resource('services', 'frontend\HomesController@services');

    Route::get('product_detail/{product_id}', 'frontend\HomesController@productDetail');

    Route::get('download_file/{pathToFile}', 'frontend\HomesController@download_file');
    Route::resource('search', 'frontend\HomesController@search');

});

###############################

Route::group(
  array('prefix'=>'admin'), function(){

    Route::get('tree', array('as'=>'category', 'uses'=> 'admin\ProjectCategoriesController@tree'));
    Route::get('getJsonTree', array('as'=>'category', 'uses'=> 'admin\ProjectCategoriesController@getJsonTree'));
});


### Check Login ###
Route::get('administrator', function()
{
    if ( ! Sentry::check())
    {
         return Redirect::route('getLogin');
    }
    else
    {
        return Redirect::route('dashboard');   
    }
});

Route::get('administrator/login', array('as'=>'getLogin', 'uses'=> 'admin\UsersController@getLogin'));
Route::post('administrator/login', array('as'=>'postLogin', 'uses'=> 'admin\UsersController@postLogin'));
Route::get('administrator/logout', array('as'=>'getLogout', 'uses'=> 'admin\UsersController@getLogout'));
Route::get('administrator/getSelectBoxSubCat/{parent_id}/{selected_id}', array('as'=>'getSubCat', 'uses'=> 'admin\ProductsController@getSelectBoxSubCat'));

### Admin Panel ###
Route::group(
  array('before'=>'auth'),

    function(){

        Route::group(
          array('prefix'=>'admin'),
            function(){

                Route::get('dashboard', array('as'=>'dashboard', 'uses'=> 'admin\DashboardController@getIndex'));
                Route::resource('job', 'admin\JobsController');
                Route::resource('link', 'admin\LinksController');
                Route::resource('customer', 'admin\CustomersController');
                Route::resource('projectcategory', 'admin\ProjectCategoriesController');
                Route::resource('project', 'admin\ProjectsController');
                Route::resource('projectgallery', 'admin\ProjectGalleriesController');

                Route::resource('activity', 'admin\ActivitiesController');
                Route::resource('activitygallery', 'admin\ActivityGalleriesController');

                Route::resource('what', 'admin\WhatsController');
                Route::resource('slider', 'admin\SlidersController');


                Route::resource('aboutus', 'admin\AboutusController');
                Route::resource('contactus', 'admin\ContactusesController');
                Route::resource('user', 'admin\UsersController');
                Route::get('news', array('as'=>'admin.news', 'uses'=>'admin\NewsController@Index'));      
                Route::post('uploadTemp', 'admin\UploadCropsController@uploadTemp');
                Route::post('uploadCrop', 'admin\UploadCropsController@uploadCrop');

            }
        );

    }
);