<?php
namespace admin;

/**
 * admin\CompanyProfile
 *
 * @property integer $id
 * @property string $msg
 * @property string $msg_en
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\admin\CompanyProfile whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\CompanyProfile whereMsg($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\CompanyProfile whereMsgEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\CompanyProfile whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\CompanyProfile whereUpdatedAt($value) 
 */
class Aboutus extends \Eloquent {
	protected $guarded = array();

    public $table = 'aboutus';

    public static $rules_update = array(
        'title'      =>'required',
        'title_en'   =>'required',
   );

}
