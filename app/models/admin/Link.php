<?php
namespace admin;

/**
 * admin\Product
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $name_en
 * @property string $short_description
 * @property string $short_description_en
 * @property string $description
 * @property string $description_en
 * @property string $image
 * @property string $image_thumb
 * @property string $fact_sheet
 * @property integer $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereCategoryId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereNameEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereShortDescription($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereShortDescriptionEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereDescription($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereDescriptionEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereImage($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereImageThumb($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereFactSheet($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereStatus($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Product whereUpdatedAt($value) 
 */
class Link extends \Eloquent {
    protected $guarded = array();

    public $table = 'links';

    public static $rules_add = array(
        'title' =>'required',
        'link_banner' =>'required'
    );

    public static $rules_update = array(
        'title' =>'required',
        'link_banner' =>'required'
    );


}
