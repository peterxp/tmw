<?php
namespace admin;

/**
 * admin\Contactus
 *
 * @property integer $id
 * @property string $company_name
 * @property string $company_name_en
 * @property string $address
 * @property string $address_en
 * @property string $province
 * @property string $province_en
 * @property string $post_code
 * @property string $tax_id
 * @property string $email
 * @property string $map
 * @property string $facebook_url
 * @property string $telephone
 * @property string $mobile
 * @property string $fax
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereCompanyName($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereCompanyNameEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereAddress($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereAddressEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereProvince($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereProvinceEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus wherePostCode($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereTaxId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereEmail($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereMap($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereFacebookUrl($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereTelephone($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereMobile($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereFax($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Contactus whereUpdatedAt($value) 
 */
class Contactus extends \Eloquent {
    protected $guarded = array();

    public $table = 'contactuses';

//   `company_name` 
//   `company_name_en` 
//   `address` 
//   `address_en` 
//   `province` 
//   `province_en` 
//   `post_code` 
//   `tax_id` 
//   `email` 
//   `map` 
//   `facebook_url`
//   `telephone` 
//   `mobile` 
//   `fax`

    public static $rules = array(
        'company_name'      =>'required',
        'company_name_en'   =>'required',
        'address'           =>'required',
        'province'          =>'required',
        'province_en'       =>'required',
        'post_code'         =>'required',
        'tax_id'            =>'required',
        'email'             =>'required',
        'map'               =>'required',
        'facebook_url'      =>'required',
        'telephone'         =>'required',
        'mobile'            =>'required',
        'fax'               =>'required'

   );

}
