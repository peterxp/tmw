<?php
namespace admin;

class Customer extends \Eloquent {
    protected $guarded = array();

    public $table = 'customers';

    public static $rules_add = array(
        'title' =>'required',
        'customer_logo' =>'required'
    );

    public static $rules_update = array(
        'title' =>'required',
        'customer_logo' =>'required'
    );


}

?>