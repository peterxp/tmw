<?php
namespace admin;

/**
 * admin\Faq
 *
 * @property integer $id
 * @property string $question
 * @property string $question_en
 * @property string $answer
 * @property string $answer_en
 * @property string $test
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\admin\Faq whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Faq whereQuestion($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Faq whereQuestionEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Faq whereAnswer($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Faq whereAnswerEn($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Faq whereTest($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Faq whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\Faq whereUpdatedAt($value) 
 */
class Job extends \Eloquent {
	protected $guarded = array();

    public $table = 'jobs';

    public static $rules = array(
        'topic'      =>'required',
        'topic_en'   =>'required',
        'detail'     =>'required',
        'detail_en'  =>'required'
   );

}
