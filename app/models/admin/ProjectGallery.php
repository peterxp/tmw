<?php
namespace admin;

/**
 * admin\ProductGallery
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $description
 * @property string $image
 * @property string $image_thumb
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\admin\ProductGallery whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\ProductGallery whereProductId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\ProductGallery whereDescription($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\ProductGallery whereImage($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\ProductGallery whereImageThumb($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\ProductGallery whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\ProductGallery whereUpdatedAt($value) 
 */
class ProjectGallery extends \Eloquent {
    protected $guarded = array();

    public $table = 'projectgalleries';

    public static $rules_add = array(
        'project_id'    =>'required',
        'image'         =>'required'
    );

    public static $rules_update = array(
        'project_id'    =>'required'
   );


}
