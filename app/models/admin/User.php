<?php
namespace admin;

/**
 * admin\User
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $permissions
 * @property boolean $activated
 * @property string $activation_code
 * @property string $activated_at
 * @property string $last_login
 * @property string $persist_code
 * @property string $reset_password_code
 * @property string $first_name
 * @property string $last_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereEmail($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User wherePassword($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User wherePermissions($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereActivated($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereActivationCode($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereActivatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereLastLogin($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User wherePersistCode($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereResetPasswordCode($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereFirstName($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereLastName($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\admin\User whereUpdatedAt($value) 
 */
class User extends \Eloquent {
	protected $guarded = array();

    public static $rules = array(
        'first_name'  =>'required',
        'last_name'   =>'required',
        'email'       =>'required'
    );
    
    public $table = 'users';

}
