<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductcategoriesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Productcategory::create([
			    'parent_id' => $faker->parent_id,
			    'category_name' => $faker->category_name,
			    'category_name_en' => $faker->category_name_en,
			    'short_description' => $faker->short_description,
			    'short_description_en' => $faker->short_description_en,
			    'short_description' => $faker->image,
			    'short_description_en' => $faker->image_thumb,
			    'status' => $faker->status
			]);
		}

	}

}