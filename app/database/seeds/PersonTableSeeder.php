<?php
class PersonTableSeeder extends Seeder {

    public function run()
    {
        $count = 75;

        $this->command->info('Deleting existing Person table ...');
        DB::table('person')->delete();

        $faker = Faker\Factory::create('en_GB');

        $faker->addProvider(new Faker\Provider\en_GB\Address($faker));
        $faker->addProvider(new Faker\Provider\en_GB\Internet($faker));
        $faker->addProvider(new Faker\Provider\Uuid($faker));

        $this->command->info('Inserting '.$count.' sample records using Faker ...');
        // $faker->seed(1234);

        for( $x=0 ; $x<$count; $x++ )
        {
            Person::create(array(
                'name' => $faker->name,
                'email' => $faker->companyEmail,
                'description' => '<p>'.  implode('</p><p>', $faker->paragraphs(5)) .'</p>',
                'country' => $faker->country,
                'address' => $faker->address,
                'postcode' => $faker->postcode,
                'telephone' => $faker->phoneNumber,
                'code' => $faker->uuid
            ));
        }

        $this->command->info('Person table seeded using Faker ...');
    }

}

?>