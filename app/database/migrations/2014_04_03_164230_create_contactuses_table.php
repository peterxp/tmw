<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactusesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contactuses', function(Blueprint $table) {
			$table->increments('id');
			$table->string('company_name');
			$table->string('company_name_en');
			$table->text('address');
			$table->text('address_en');
			$table->string('province');
			$table->string('province_en');
			$table->string('post_code');
			$table->string('tax_id');
			$table->string('email');
			$table->string('map');
			$table->string('facebook_url');
			$table->string('telephone');
			$table->string('mobile');
			$table->string('fax');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contactuses');
	}

}
