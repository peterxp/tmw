<?php

return array(

    'upload'  =>array(
        'temp_dir' =>public_path() . '/upload/temp_dir/'
    ),

    'activity'  => array(
        'width'         =>640,
        'height'        =>427,
        'width_thumb'   =>200,
        'height_thumb'  =>133,
        'target_path'   =>public_path() . '/upload/images/activity/'
    ),
    
    'activitygallery'=> array(
        'width'         =>640,
        'height'        =>427,
        'width_thumb'   =>100,
        'height_thumb'  =>100,
        'target_path'    =>public_path() . '/upload/images/activitygallery/'
    ),

    'slider'=> array(
        'width'         =>1024,
        'height'        =>500,
        'width_thumb'   =>1024,
        'height_thumb'  =>500,
        'target_path'    =>public_path() . '/upload/images/slider/'
    ),

    'what_image'=> array(
        'width'         =>665,
        'height'        =>285,
        'width_thumb'   =>665,
        'height_thumb'  =>285,
        'target_path'    =>public_path() . '/upload/images/what-we-do/'
    ),

    'project' => array(
        'width'         =>325,
        'height'        =>217,
        'width_thumb'   =>200,
        'height_thumb'  =>133,
        'target_path'    =>public_path() . '/upload/images/project/'
    ),

    'projectgallery' => array(
        'width'         =>640,
        'height'        =>427,
        'width_thumb'   =>100,
        'height_thumb'  =>100,
        'target_path'    =>public_path() . '/upload/images/projectgallery/'
    ),


    // 'productcategory_banner' => array(
    //     'width'         =>555,
    //     'height'        =>125,
    //     'banner_path'    =>public_path() . '/upload/images/productcategory/'
    // ),

    'projectcategory_icon' => array(
        'width'         =>140,
        'height'        =>140,
        'target_path'    =>public_path() . '/upload/images/projectcategory_icon/'
    ),

    'product' => array(
        'width'         =>800,
        'height'        =>600,
        'width_thumb'   =>170,
        'height_thumb'  =>115,
        'target_path'    =>public_path() . '/upload/images/product/'
    ),



    'link_banner' => array(
        'width'         =>325,
        'height'        =>103,
        'target_path'    =>public_path() . '/upload/images/link_banner/'
    ),

    'contactus' => array(
        'width'         =>1000,
        'height'        =>450,
        'target_path'    =>public_path() . '/upload/images/contactus/'
    ),

    'portfolio_icon' => array(
        'width'         =>140,
        'height'        =>140,
        'target_path'    =>public_path() . '/upload/images/portfolio_icon/'
    ),

    'customer_logo' => array(
        'width'         =>200,
        'height'        =>133,
        'target_path'    =>public_path() . '/upload/images/customer_logo/'
    ),

    'aboutus_image' => array(
        'width'         =>665,
        'height'        =>382,
        'target_path'    =>public_path() . '/upload/images/aboutus/'
    ),

    'aboutus_image_n' => array(
        'width'         =>150,
        'height'        =>100,
        'target_path'    =>public_path() . '/upload/images/aboutus/'
    )


);