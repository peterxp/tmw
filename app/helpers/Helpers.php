<?php

class Helpers{

    public static function formatUrl($url, $sep='-')
    {
        return preg_replace("![^a-z0-9&]+!i", $sep, $url);
    }

    public static function selectBox($arr, $selected_id, $msg = "Please Select")
    {
        $txt = "";
        $txt .="<option value=''>".$msg."</option>";
        foreach ($arr as $key => $value) {
            $selected = ($value->id == $selected_id)? "selected":"";
            $txt .= "<option value='".$value->id."' ".$selected.">".$value->category_name."</option>";
        }
        return $txt;
    }

    public static function getFileExtension($file_name)
    {
        return substr(strrchr($file_name,'.'),1);
    }

}
