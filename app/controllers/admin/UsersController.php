<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use Hash;


class UsersController extends \BaseController {


    public function index()
    {
        $id = Sentry::getUser()->id;
        $data = User::find($id); 
        return \View::make('admin.user.show')->with('data', $data);
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		$key = Session::put('key', 'value');
        return \View::make('admin.login.index')->with('data', $key);
	}

	/**
	* @retrun Response/Redirect
	* Post Check Login and assign session
	*/
	public function postLogin()
  	{
        $credentials = array(
            'email'    => Input::get('email'),
            'password' => Input::get('password')
        );

        try
        {
            $user = Sentry::authenticate($credentials, false);

            if ($user)
            {

                ## Create Session & Redirect
              	return Redirect::route('dashboard');
            }

        }
		catch (\Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    $login_error = 'Login field is required.';
		    return Redirect::to('administrator/login')->with('login_error', $login_error)->withInput();
		}
		catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    $login_error = 'Password field is required.';
		    return Redirect::to('administrator/login')->with('login_error', $login_error)->withInput();
		}
		catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    $login_error = 'Your username/password combination was incorrect.';
		    return Redirect::to('administrator/login')->with('login_error', $login_error)->withInput();
		}
	}

	/**
	 * Logout.
	 *
	 * @return Response
	 */
	public function getLogout()
	{	
        Sentry::logout();
        return Redirect::to('administrator/login');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('logins.create');

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return View::make('logins.show');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function edit($id)
    {
        $data = User::find($id);

        if (is_null($data))
        {
            return Redirect::route('admin.user.index');
        }

        return \View::make('admin.user.edit')->with('data', $data);

    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        // var_dump(Input::all());
        // exit();
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, User::$rules);

        if ($validation->passes())
        {
            $rec = Sentry::find($id);
            $rec->first_name    = Input::get('first_name');
            $rec->last_name     = Input::get('last_name');
            $rec->email         = Input::get('email');

            if(Input::get('password') != ""){
                $rec->password         = Input::get('password');
            }

            $rec->save();

            return Redirect::route('admin.user.index');
        }

        return Redirect::route('admin.user.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
