<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;


class DashboardController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {

        $project_category_count = ProjectCategory::count();
        $project_count          = Project::count();
        $activity_count         = Activity::count();
        $ourcustomer_count      = Customer::count();
        $job_vacancy_count      = Job::count();

        $data = array(
                    'project_category_count'=> $project_category_count,
                    'project_count'         => $project_count,
                    'activity_count'        => $activity_count,
                    'ourcustomer_count'     => $ourcustomer_count, 
                    'job_vacancy_count'     => $job_vacancy_count                                       
                );

        return \View::make('admin.dashboard.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('logins.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return View::make('logins.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return View::make('logins.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
