<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;


class JobsController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $jobs = Job::paginate(10);
        return \View::make('admin.job.index')->with('jobs', $jobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return \View::make('admin.job.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $validation = Validator::make(Input::all(), Job::$rules);
        if($validation->fails())
        {
            return Redirect::route('admin.job.create')
                ->withErrors($validation)
                ->withInput();
        }
        $exceptFields = array();
        Job::create(Input::except($exceptFields));
      
        return Redirect::route('admin.job.index');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $job = Job::find($id);
        return \View::make('admin.job.show')->with('job', $job);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $job = Job::find($id);
        return \View::make('admin.job.edit')->with('job', $job);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Job::$rules);

        if ($validation->passes())
        {
            $job = Job::find($id);
            $job->update($input);

            return Redirect::route('admin.job.show', $id);
        }

        return Redirect::route('admin.job.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Job::find($id)->delete();
        return Redirect::route('admin.job.index');
    }

}