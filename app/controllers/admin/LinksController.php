<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;
use Response;
use Helpers;

class LinksController extends \BaseController {
  
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Link::select()
            ->orderBy('id','DESC')
            ->get();

        return \View::make('admin.link.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return \View::make('admin.link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $validation = Validator::make(Input::all(), Link::$rules_add);
        if($validation->fails())
        {
            return Redirect::route('admin.link.create')
                ->withErrors($validation)
                ->withInput();
        }
        $exceptFields = array('link_banner');
        $rec = Link::create(Input::except($exceptFields));
        $lastInsertedId =  $rec->id;
        
        ## Success
        if($lastInsertedId){

            if(Input::hasFile('link_banner')){

                $extension = Input::file('link_banner')->getClientOriginalExtension();
                ### config.with, config.height
                $target_path        = Config::get('admin.link_banner.target_path');
                $configWidth        = Config::get('admin.link_banner.width');
                $configHeight       = Config::get('admin.link_banner.height');   

                $filename = $lastInsertedId."_".date("dmY-His") . '.' . $extension;
                $img = Image::make(Input::file('link_banner')->getRealPath())
                    ->resize($configWidth, $configHeight, true)
                    ->save($target_path . $filename);

                $data = Link::find($lastInsertedId);              
                $data->link_banner = $filename;
                $data->save();
            }

            ## 
            return Redirect::route('admin.link.index');
        }

        ## Fail
        Redirect::route('admin.link.create')
            ->withErrors($validation)
            ->withInput();           
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $data = Link::find($id);
        return \View::make('admin.link.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Link::find($id);
        return \View::make('admin.link.edit')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $validation = Validator::make(Input::all(), Link::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.link.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('link_banner');

        $rec = Link::find($id);
        $rec->update(Input::except($exceptFields));
        $lastInsertedId =  $rec->id;

        # Success
        if($lastInsertedId){

            if(Input::hasFile('link_banner')){

                ### config.with, config.height
                $target_path        = Config::get('admin.link_banner.target_path');
                $configWidth        = Config::get('admin.link_banner.width');
                $configHeight       = Config::get('admin.link_banner.height');   

                $filename = $rec->link_banner;
                $img = Image::make(Input::file('link_banner')->getRealPath())
                    ->resize($configWidth, $configHeight, true)
                    ->save($target_path . $filename);

                $dat = Link::find($lastInsertedId);              
                $dat->link_banner = $filename;
                $dat->save();

            }

            ## 
            return Redirect::route('admin.link.index');
        }

        ## Fail
        Redirect::route('admin.link.edit', $id)
            ->withErrors($validation)
            ->withInput();     
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = Link::find($id);
        File::delete(array(
            Config::get('admin.link_banner.target_path').$data->link_banner
        ));

        Link::find($id)->delete();

        return Redirect::route('admin.link.index');
    }

}