<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;
use Response;
use Helpers;
// use ProjectCategoriesController;

class ProjectsController extends \BaseController {
  
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $datas = Project::select(['projects.*','pc.title as category_title'])
            ->leftJoin('projectcategories as pc', 'projects.projectcategories_id', '=', 'pc.id')
            ->orderBy('projects.id','DESC')
            ->get();

        return \View::make('admin.project.index')->with('datas', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $projectCategory = $this->getJsonTree();

        return \View::make('admin.project.create')->with('categories', $projectCategory);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $validation = Validator::make(Input::all(), Project::$rules_add);
        if($validation->fails())
        {
            return Redirect::route('admin.project.create')
                ->withErrors($validation)
                ->withInput();
        }
        $exceptFields = array('width_image', 'height_image', 'width_thumb', 'height_thumb', 'img', 'x1', 'x2', 'y1', 'y2', 'w', 'h', 'main_portfolio');
        $rec = Project::create(Input::except($exceptFields));
        $lastInsertedId =  $rec->id;
        
        ## Success
        if($lastInsertedId){

            if(Input::has('image')){

                ### config.with, config.height
                $temp_dir           = Config::get('admin.upload.temp_dir');
                $target_path        = Config::get('admin.project.target_path');


                ## Asssing file name
                $image = Input::get('image');
                $image_thumb = Input::get('image_thumb');

                ## Save image as target
                $img = Image::make($temp_dir . $image);
                $img->save($target_path . $image);

                ## Save image thumb as target
                $img = Image::make($temp_dir . $image_thumb);
                $img->save($target_path . $image_thumb);

                ## Update Image Name  
                $data = Project::find($lastInsertedId);              
                $data->image        = $image;
                $data->image_thumb  = $image_thumb;
                $data->save();

            }

            ## 
            return Redirect::route('admin.project.index');
        }

        ## Fail
        Redirect::route('admin.project.create')
            ->withErrors($validation)
            ->withInput();           
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $datas = Project::select(['projects.*','pc.title as category_title'])
            ->Join('projectcategories as pc', 'projects.projectcategories_id', '=', 'pc.id')
            ->where('projects.id', $id)
            ->orderBy('projects.id','DESC')
            ->get();

        return \View::make('admin.project.show')->with('data', $datas[0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $data = Project::select(['projects.*','pc.title as category_title', 'pc.title_en as category_title_en'])
            ->Join('projectcategories as pc', 'projects.projectcategories_id', '=', 'pc.id')
            ->where('projects.id', $id)
            ->orderBy('projects.id','DESC')
            ->get();

        if (is_null($data[0]))
        {
            return Redirect::route('admin.project.index');
        }

        $projectCategory = $this->getJsonTree();

        return \View::make('admin.project.edit')
            ->with('data', $data[0])
            ->with('categories', $projectCategory);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $validation = Validator::make(Input::all(), Project::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.project.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('image', 'image_thumb', 'width_image', 'height_image', 'width_thumb', 'height_thumb', 'x1', 'x2', 'y1', 'y2', 'w', 'h');

        $rec = Project::find($id);
        $rec->update(Input::except($exceptFields));
        $lastInsertedId =  $id;

        if(Input::hasFile('img')){

            ### config.with, config.height
            $temp_dir           = Config::get('admin.upload.temp_dir');
            $target_path         = Config::get('admin.project.target_path');

            ## Asssing file name
            $image = Input::get('image');
            $image_thumb = Input::get('image_thumb');

            ## Save image as target
            $img = Image::make($temp_dir . $image);
            $img->save($target_path . $image);

            ## Save image thumb as target
            $img_thumb = Image::make($temp_dir . $image_thumb);
            $img_thumb->save($target_path . $image_thumb);
       
            ## Update Image Name  
            $data = Project::find($lastInsertedId);              
            $data->image         = $image;
            $data->image_thumb   = $image_thumb;
            $data->save();

        }
        
        return Redirect::route('admin.project.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = Project::find($id);
        File::delete(array(
            Config::get('admin.project.target_path').$data->image, 
            Config::get('admin.project.target_path').$data->image_thumb
        ));

        Project::find($id)->delete();

        return Redirect::route('admin.project.index');
    }



    /**
    * Create Tree have 2 function createBranch(), createTree
    **/
    function createBranch(&$parents, $children) 
    {
        $tree = array();
        foreach ($children as $child) 
        {
            if (isset($parents[$child['id']])) 
            {
                $child['children'] = $this->createBranch($parents, $parents[$child['id']]);
            }
            $tree[] = $child;
        } 
        return $tree;
    }

    function createTree($flat, $root = 0) 
    {
        $parents = array();
        foreach ($flat as $a) 
        {
            $parents[$a['parent_id']][] = $a;
        }
        return $this->createBranch($parents, $parents[$root]);
    }


    public function getJsonTree(){

        $cat = DB::table('projectcategories')->select(['*'])
            ->orderBy('parent_id', 'ASC')
            ->orderBy('title_en', 'ASC')
            ->get();

        # Prepare Array
        $arr = array();
        foreach ($cat as $key => $value) {
            $arr[$key] = array(
                'id'        =>$value->id,
                'parent_id' =>$value->parent_id,
                'title'     =>$value->title,
                'title_en'  =>$value->title_en,
                'description'       =>$value->description,
                'description_en'    =>$value->description_en,
                'image'             =>$value->image
            );
         }

        return $this->createTree($arr);

    }
}