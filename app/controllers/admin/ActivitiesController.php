<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;
use Response;
use Helpers;

class ActivitiesController extends \BaseController {
  
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $datas = Activity::select()
            ->orderBy('id','DESC')
            ->get();

        return \View::make('admin.activity.index')->with('datas', $datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return \View::make('admin.activity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $validation = Validator::make(Input::all(), Activity::$rules_add);
        if($validation->fails())
        {
            return Redirect::route('admin.activity.create')
                ->withErrors($validation)
                ->withInput();
        }
        $exceptFields = array('width_image', 'height_image', 'width_thumb', 'height_thumb', 'img', 'x1', 'x2', 'y1', 'y2', 'w', 'h');
        $rec = Activity::create(Input::except($exceptFields));
        $lastInsertedId =  $rec->id;
        
        ## Success
        if($lastInsertedId){

            if(Input::has('image_thumb')){

                ### config.with, config.height
                $temp_dir           = Config::get('admin.upload.temp_dir');
                $target_path        = Config::get('admin.activity.target_path');

                ## Asssing file name
                $image = Input::get('image');
                $image_thumb = Input::get('image_thumb');

                ## Save image as target
                $img = Image::make($temp_dir . $image);
                $img->save($target_path . $image);

                ## Save image thumb as target
                $img = Image::make($temp_dir . $image_thumb);
                $img->save($target_path . $image_thumb);

                ## Update Image Name  
                $data = Activity::find($lastInsertedId);              
                $data->image        = $image;
                $data->image_thumb  = $image_thumb;
                $data->save();

            }

            ## 
            return Redirect::route('admin.activity.index');
        }

        ## Fail
        Redirect::route('admin.activity.create')
            ->withErrors($validation)
            ->withInput();           
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $datas = Activity::select()
            ->where('id', $id)
            ->orderBy('id','DESC')
            ->get();

        return \View::make('admin.activity.show')->with('data', $datas[0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $data = Activity::select()
            ->where('id', $id)
            ->orderBy('id','DESC')
            ->get();

        return \View::make('admin.activity.edit')->with('data', $data[0]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        // print_r($_POST);
        // exit();

        $validation = Validator::make(Input::all(), Activity::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.activity.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('image', 'image_thumb', 'width_image', 'height_image', 'width_thumb', 'height_thumb', 'x1', 'x2', 'y1', 'y2', 'w', 'h');

        $rec = Activity::find($id);
        $rec->update(Input::except($exceptFields));
        $lastInsertedId =  $id;

        # Success
        if(Input::hasFile('img')){

            ### config.with, config.height
            $temp_dir           = Config::get('admin.upload.temp_dir');
            $target_path        = Config::get('admin.activity.target_path');

            ## Asssing file name
            $image = Input::get('image');
            $image_thumb = Input::get('image_thumb');

            ## Save image as target
            $img = Image::make($temp_dir . $image);
            $img->save($target_path . $image);

            ## Save image thumb as target
            $img = Image::make($temp_dir . $image_thumb);
            $img->save($target_path . $image_thumb);

            ## Update Image Name  
            $data = Activity::find($lastInsertedId);              
            $data->image  = $image;
            $data->image_thumb  = $image_thumb;
            $data->save();
        }
        return Redirect::route('admin.activity.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = Activity::find($id);
        File::delete(array(
            Config::get('admin.activity.target_path').$data->image, 
            Config::get('admin.activity.target_path').$data->image_thumb
        ));

        Activity::find($id)->delete();

        return Redirect::route('admin.activity.index');
    }

}