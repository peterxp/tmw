<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;

class UploadCropsController extends \BaseController {
  
    public function uploadTemp()
    {

        $image_path = public_path() . '/upload/temp_dir/';

        if(Input::hasFile('img')){

            $extension = Input::file('img')->getClientOriginalExtension();
            $image_name = date("dmY-His") . '.' . $extension;

            $img = Image::make(Input::file('img')->getRealPath());
            $img->resize(Input::get('width_image'), null, true, true); // ratio=true, upsize=false
            $img->save($image_path . $image_name);

            $image = $image_path . $image_name;

            $data = array(
                'img'=> $image_name,
                'status' =>'Y'
            );

        }else{
            $data = array('status' =>'Y');
        }
        return json_encode($data);
    }

    public function uploadCrop()
    {
        
        $image = Input::get('image');
        $image_thumb = 'thumb_' . Input::get('image');
        $image_path = public_path() . '/upload/temp_dir/';

        $img = Image::make($image_path . $image);
        $img->crop(Input::get('w'), Input::get('h'), Input::get('x1'), Input::get('y1'));
        $img->resize(Input::get('width_thumb'), null,  true, true); // ratio=true, upsize=false
        $img->save($image_path . $image_thumb);

        $data = array(
            'image_thumb'=>$image_thumb
        );
      
        return($data);

    }

}