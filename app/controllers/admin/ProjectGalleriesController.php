<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;

class ProjectGalleriesController extends \BaseController {
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     * Make show.blade.php as crate
     */
    public function show($project_id)
    {

        $datas = ProjectGallery::where('project_id', '=', $project_id)->get();

        return \View::make('admin.projectgallery.show')
            ->with('datas', $datas)
            ->with('project_id', $project_id);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // var_dump(Input::all());
        // exit();

        $validation = Validator::make(Input::all(), ProjectGallery::$rules_add);
        if($validation->fails())
        {
            return Redirect::route('admin.projectgallery.show', Input::get('project_id'))
                ->withErrors($validation)
                ->withInput();
        }

        $data                = new ProjectGallery;
        $data->project_id    = Input::get('project_id');
        $data->save();

        if(Input::has('image')){

            ### config.with, config.height
            $lastInsertedId = $data->id;
            $temp_dir           = Config::get('admin.upload.temp_dir');
            $target_path        = Config::get('admin.projectgallery.target_path');
            
            ## Asssing file name
            $image = Input::get('image');
            $image_thumb = Input::get('image_thumb');

            ## Save image as target
            $img = Image::make($temp_dir . $image)
              ->save($target_path . $image);
            
            ## Save image thumb as target
            $img_thumb = Image::make($temp_dir . $image_thumb)
                ->save($target_path . $image_thumb);

            ## Update Image Name  
            $data = ProjectGallery::find($lastInsertedId);              
            $data->image         = $image;
            $data->image_thumb   = $image_thumb;
            $data->save();

        }

        return Redirect::route('admin.projectgallery.show', Input::get('project_id'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $data = ProjectGallery::where('id', '=', $id)->get();

        // print_r($data);
        // exit();

        if (is_null($data[0]))
        {
            return Redirect::back()->withInput();
        }

        return \View::make('admin.projectgallery.edit')->with('data', $data[0]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
  
        $validation = Validator::make(Input::all(), ProjectGallery::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.projectgallery.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('image', 'image_thumb', 'width_image', 'height_image', 'width_thumb', 'height_thumb', 'x1', 'x2', 'y1', 'y2', 'w', 'h');
        $data = ProjectGallery::find($id);
        $data->update(Input::except($exceptFields));

        if(Input::hasFile('img')){

            ### config.with, config.height
            $lastInsertedId = $data->id;
            $temp_dir           = Config::get('admin.upload.temp_dir');
            $target_path         = Config::get('admin.projectgallery.target_path');
            
            ## Asssing file name
            $image = Input::get('image');
            $image_thumb = Input::get('image_thumb');

            ## Save image as target
            $img = Image::make($temp_dir . $image);
            $img->save($target_path . $image);
            
            ## Save image thumb as target
            $img_thumb = Image::make($temp_dir . $image_thumb);
            $img_thumb->save($target_path . $image_thumb);

            ## Update Image Name  
            $data = ProjectGallery::find($lastInsertedId);              
            $data->image         = $image;
            $data->image_thumb   = $image_thumb;
            $data->save();

        }

        return Redirect::route('admin.projectgallery.show', Input::get('project_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $data = ProjectGallery::find($id);
      File::delete(array(
            Config::get('admin.projectgallery.target_path').$data->image, 
            Config::get('admin.projectgallery.target_path').$data->image_thumb
        ));

        ProjectGallery::find($id)->delete();

        return Redirect::back();
    }



}