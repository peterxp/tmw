<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use Str;

class AboutusController extends \BaseController {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $data = Aboutus::find(1);
        
        return \View::make('admin.aboutus.index')->with('data', $data);
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Aboutus::find($id);

        if (is_null($data))
        {
            return Redirect::route('admin.aboutus.index');
        }

        return \View::make('admin.aboutus.edit')->with('data', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $validation = Validator::make(Input::all(), Aboutus::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.aboutus.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('image', 'image1', 'image2', 'image3', 'image4');

        $rec = Aboutus::find($id);
        $rec->update(Input::except($exceptFields));
        $lastInsertedId =  $id;

        # Success
        if(Input::hasFile('image')){

            ### config.with, config.height
            $target_path        = Config::get('admin.aboutus_image.target_path');
            $configWidth        = Config::get('admin.aboutus_image.width');
            $configHeight       = Config::get('admin.aboutus_image.height');   

            $extension = Input::file('image')->getClientOriginalExtension();
            $filename = Str::random(8)."_".date("dmY-His") . '.' . $extension;

            $img = Image::make(Input::file('image')->getRealPath())
                ->resize($configWidth, $configHeight, true)
                ->save($target_path . $filename);

            $dat = Aboutus::find($lastInsertedId);              
            $dat->image = $filename;
            if($dat->save()){
                File::delete(array(
                    Config::get('admin.aboutus_image.target_path').$rec->image
                ));
            }
        }

        # Success
        if(Input::hasFile('image1')){

            ### config.with, config.height
            $target_path        = Config::get('admin.aboutus_image_n.target_path');
            $configWidth        = Config::get('admin.aboutus_image_n.width');
            $configHeight       = Config::get('admin.aboutus_image_n.height');   

            $extension = Input::file('image1')->getClientOriginalExtension();
            $filename = Str::random(8)."_".date("dmY-His") . '.' . $extension;

            $img = Image::make(Input::file('image1')->getRealPath())
                ->resize($configWidth, $configHeight, true)
                ->save($target_path . $filename);

            $dat = Aboutus::find($lastInsertedId);              
            $dat->image1 = $filename;
            if($dat->save()){
                File::delete(array(
                    Config::get('admin.aboutus_image.target_path').$rec->image1
                ));
            }
        }

        # Success
        if(Input::hasFile('image2')){

            ### config.with, config.height
            $target_path        = Config::get('admin.aboutus_image_n.target_path');
            $configWidth        = Config::get('admin.aboutus_image_n.width');
            $configHeight       = Config::get('admin.aboutus_image_n.height');   

            $extension = Input::file('image2')->getClientOriginalExtension();
            $filename = Str::random(8)."_".date("dmY-His") . '.' . $extension;

            $img = Image::make(Input::file('image2')->getRealPath())
                ->resize($configWidth, $configHeight, true)
                ->save($target_path . $filename);

            $dat = Aboutus::find($lastInsertedId);              
            $dat->image2 = $filename;
            if($dat->save()){
                File::delete(array(
                    Config::get('admin.aboutus_image.target_path').$rec->image2
                ));
            }
        }

        # Success
        if(Input::hasFile('image3')){

            ### config.with, config.height
            $target_path        = Config::get('admin.aboutus_image_n.target_path');
            $configWidth        = Config::get('admin.aboutus_image_n.width');
            $configHeight       = Config::get('admin.aboutus_image_n.height');   

            $extension = Input::file('image3')->getClientOriginalExtension();
            $filename = Str::random(8)."_".date("dmY-His") . '.' . $extension;

            $img = Image::make(Input::file('image3')->getRealPath())
                ->resize($configWidth, $configHeight, true)
                ->save($target_path . $filename);

            $dat = Aboutus::find($lastInsertedId);              
            $dat->image3 = $filename;
            if($dat->save()){
                File::delete(array(
                    Config::get('admin.aboutus_image.target_path').$rec->image3
                ));
            }
        }

        # Success
        if(Input::hasFile('image4')){

            ### config.with, config.height
            $target_path        = Config::get('admin.aboutus_image_n.target_path');
            $configWidth        = Config::get('admin.aboutus_image_n.width');
            $configHeight       = Config::get('admin.aboutus_image_n.height');   

            $extension = Input::file('image4')->getClientOriginalExtension();
            $filename = Str::random(8)."_".date("dmY-His") . '.' . $extension;

            $img = Image::make(Input::file('image4')->getRealPath())
                ->resize($configWidth, $configHeight, true)
                ->save($target_path . $filename);

            $dat = Aboutus::find($lastInsertedId);              
            $dat->image4 = $filename;
            if($dat->save()){
                File::delete(array(
                    Config::get('admin.aboutus_image.target_path').$rec->image4
                ));
            }
        }


        return Redirect::route('admin.aboutus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = Aboutus::find($id);
        @File::delete(array(
            Config::get('admin.aboutus_image.target_path').$data->image,
            Config::get('admin.aboutus_image.target_path').$data->image1,
            Config::get('admin.aboutus_image.target_path').$data->image2,
            Config::get('admin.aboutus_image.target_path').$data->image3,
            Config::get('admin.aboutus_image.target_path').$data->image4
        ));

        return Redirect::route('admin.aboutus.index');
    }

}