<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;
use Response;
use Helpers;

class ProjectCategoriesController extends \BaseController {
  
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = array("jsonURL"=>"/admin/getJsonTree");
        return \View::make('admin.projectcategory.index')->with('data', (object)$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $parent = ProjectCategory::select(['id','title_en'])
            ->where('parent_id', 0)
            ->orderBy('title_en','ASC')
            ->get();

        # Prepare Array
        $arrSelectBox = array();
        $arrSelectBox[0] = "Make a root category";
        foreach ($parent as $key => $value) {
            $arrSelectBox[$value->id] = $value->title_en;
        }

        $itemSelected = 0;

        return \View::make('admin.projectcategory.create')
                ->with('itemSelected', $itemSelected)
                ->with('arrSelectBox', $arrSelectBox);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = Validator::make(Input::all(), ProjectCategory::$rules_add);
        if($validation->fails())
        {
            return Redirect::route('admin.projectcategory.create')
                ->withErrors($validation)
                ->withInput();
        }
        $exceptFields = array();
        $rec = ProjectCategory::create(Input::except($exceptFields));
        $lastInsertedId =  $rec->id;
        
        ## Success
        if($lastInsertedId){

            if(Input::hasFile('image')){

                $extension = Input::file('image')->getClientOriginalExtension();
                ### config.with, config.height
                $target_path        = Config::get('admin.projectcategory_icon.target_path');
                $configWidth        = Config::get('admin.projectcategory_icon.width');
                $configHeight       = Config::get('admin.projectcategory_icon.height');   

                $filename = $lastInsertedId."_".date("dmY-His") . '.' . $extension;
                $img = Image::make(Input::file('image')->getRealPath())
                    ->resize($configWidth, $configHeight, true)
                    ->save($target_path . $filename);

                $data = ProjectCategory::find($lastInsertedId);              
                $data->image = $filename;
                $data->save();
            }

            ## 
            return Redirect::route('admin.projectcategory.index');
        }

        ## Fail
        Redirect::route('admin.projectcategory.create')
            ->withErrors($validation)
            ->withInput();           
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

        $data = DB::table('projectcategories')
                ->where('projectcategories.id', $id)
                ->select('projectcategories.*', 'tb_parent.title_en')
                ->leftJoin('projectcategories as tb_parent', 'tb_parent.id', '=', 'projectcategories.parent_id')
                ->get();
        return \View::make('admin.projectcategory.show')->with('data', $data[0]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $parent = ProjectCategory::select(['id','title_en'])
            ->where('id', '!=', $id)
            ->where('parent_id', 0)
            ->orderBy('title_en','ASC')
            ->get();

        # Prepare Array
        $arrSelectBox = array();
        $arrSelectBox[0] = "Make a root category";
        foreach ($parent as $key => $value) {
            $arrSelectBox[$value->id] = $value->title_en;
        }

        $data = ProjectCategory::find($id);

        if (is_null($data))
        {
            return Redirect::route('admin.projectcategory.index');
        }

        return \View::make('admin.projectcategory.edit')
            ->with('data', $data)
            ->with('arrSelectBox', $arrSelectBox);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $validation = Validator::make(Input::all(), ProjectCategory::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.projectcategory.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('image');

        $rec = ProjectCategory::find($id);
        $rec->update(Input::except($exceptFields));
        $lastInsertedId =  $rec->id;

        # Success
        if($lastInsertedId){

            if(Input::hasFile('image')){

                $extension = Input::file('image')->getClientOriginalExtension();
                ### config.with, config.height
                $target_path        = Config::get('admin.projectcategory_icon.target_path');
                $configWidth        = Config::get('admin.projectcategory_icon.width');
                $configHeight       = Config::get('admin.projectcategory_icon.height');   

                $filename = $lastInsertedId."_".date("dmY-His") . '.' . $extension;
                $img = Image::make(Input::file('image')->getRealPath())
                    ->resize($configWidth, $configHeight, true)
                    ->save($target_path . $filename);

                $dat = ProjectCategory::find($lastInsertedId);              
                $dat->image = $filename;
                $dat->save();

            }

            ## 
            return Redirect::route('admin.projectcategory.index');
        }

        ## Fail
        Redirect::route('admin.projectcategory.edit', $id)
            ->withErrors($validation)
            ->withInput();     
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = ProjectCategory::find($id);
        File::delete(array(
            Config::get('admin.projectcategory_icon.target_path').$data->image
        ));

        ProjectCategory::find($id)->delete();

        return Redirect::route('admin.projectcategory.index');
    }



    /**
    * Create Tree have 2 function createBranch(), createTree
    **/
    function createBranch(&$parents, $children) 
    {
        $tree = array();
        foreach ($children as $child) 
        {
            if (isset($parents[$child['id']])) 
            {
                $child['children'] =
                    $this->createBranch($parents, $parents[$child['id']]);
            }
            $tree[] = $child;
        } 
        return $tree;
    }

    function createTree($flat, $root = 0) 
    {
        $parents = array();
        foreach ($flat as $a) 
        {
            $parents[$a['parent_id']][] = $a;
        }
        return $this->createBranch($parents, $parents[$root]);
    }


    public function getJsonTree(){

        $cat = ProjectCategory::select(['id', 'parent_id', 'title_en'])
            ->orderBy('parent_id', 'ASC')
            ->orderBy('title_en', 'ASC')
            ->get();

        # Prepare Array
        $arr = array();
        foreach ($cat as $key => $value) {
            $arr[$key] = array(
                'id'=>$value->id,
                'parent_id'=>$value->parent_id,
                'name'=>$value->title_en,
            );
         }

        return $this->createTree($arr);

    }
}