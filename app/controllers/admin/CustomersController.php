<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;
use Response;
use Helpers;

class CustomersController extends \BaseController {
  
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Customer::select()
            ->orderBy('id','DESC')
            ->get();

            // print_r($data);
            // exit();

        return \View::make('admin.customer.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return \View::make('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $validation = Validator::make(Input::all(), Customer::$rules_add);
        if($validation->fails())
        {
            return Redirect::route('admin.customer.create')
                ->withErrors($validation)
                ->withInput();
        }
        $exceptFields = array('customer_logo');
        $rec = Customer::create(Input::except($exceptFields));
        $lastInsertedId =  $rec->id;
        
        ## Success
        if($lastInsertedId){

            if(Input::hasFile('customer_logo')){

                $extension = Input::file('customer_logo')->getClientOriginalExtension();
                ### config.with, config.height
                $target_path        = Config::get('admin.customer_logo.target_path');
                $configWidth        = Config::get('admin.customer_logo.width');
                $configHeight       = Config::get('admin.customer_logo.height');   

                $filename = $lastInsertedId."_".date("dmY-His") . '.' . $extension;
                $img = Image::make(Input::file('customer_logo')->getRealPath())
                    ->resize($configWidth, $configHeight, true)
                    ->save($target_path . $filename);

                $data = Customer::find($lastInsertedId);              
                $data->customer_logo = $filename;
                $data->save();
            }

            ## 
            return Redirect::route('admin.customer.index');
        }

        ## Fail
        Redirect::route('admin.customer.create')
            ->withErrors($validation)
            ->withInput();           
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $data = Customer::find($id);
        return \View::make('admin.customer.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = Customer::find($id);
        return \View::make('admin.customer.edit')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $validation = Validator::make(Input::all(), Customer::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.customer.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('customer_logo');

        $rec = Customer::find($id);
        $rec->update(Input::except($exceptFields));
        $lastInsertedId =  $id;

        # Success
        if($lastInsertedId){

            if(Input::hasFile('customer_logo')){

                ### config.with, config.height
                $target_path        = Config::get('admin.customer_logo.target_path');
                $configWidth        = Config::get('admin.customer_logo.width');
                $configHeight       = Config::get('admin.customer_logo.height');   

                #delete old file
                File::delete(array(
                    Config::get('admin.customer_logo.target_path').$rec->customer_logo
                ));

                $extension = Input::file('customer_logo')->getClientOriginalExtension();
                $filename = $lastInsertedId."_".date("dmY-His") . '.' . $extension;
                $img = Image::make(Input::file('customer_logo')->getRealPath())
                    ->resize($configWidth, $configHeight, true)
                    ->save($target_path . $filename);

                $dat = Customer::find($lastInsertedId);              
                $dat->customer_logo = $filename;
                $dat->save();

            }

            ## 
            return Redirect::route('admin.customer.index');
        }

        ## Fail
        Redirect::route('admin.customer.edit', $id)
            ->withErrors($validation)
            ->withInput();     
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = Customer::find($id);
        File::delete(array(
            Config::get('admin.customer_logo.target_path').$data->customer_logo
        ));

        Customer::find($id)->delete();

        return Redirect::route('admin.customer.index');
    }

}