<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;

class TreesController extends \BaseController  {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return 'good';
    }

     /**
    * Create Tree have 2 function createBranch(), createTree
    **/
    public function createBranch(&$parents, $children) 
    {
        $tree = array();
        foreach ($children as $child) 
        {
            if (isset($parents[$child['id']])) 
            {
                $child['children'] =
                    $this->createBranch($parents, $parents[$child['id']]);
            }
            $tree[] = $child;
        } 
        return $tree;
    }

    public function createTree($flat, $root = 0) 
    {
        $parents = array();
        foreach ($flat as $a) 
        {
            $parents[$a['parent_id']][] = $a;
        }
        return $this->createBranch($parents, $parents[$root]);
    }

}