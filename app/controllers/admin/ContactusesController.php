<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;

class ContactusesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $data = Contactus::find(1);
        return \View::make('admin.contactus.index')->with('data', $data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = Contactus::find($id);

        return \View::make('admin.contactus.show')->with('data', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data = Contactus::find($id);

        return \View::make('admin.contactus.edit')->with('data', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($id)
    {
  
        $validation = Validator::make(Input::all(), Contactus::$rules);
        if($validation->fails())
        {
            Redirect::route('admin.contactus.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }
        $exceptFields = array('img');
        $rec = Contactus::find($id);
        $rec->update(Input::except($exceptFields));

        if(Input::hasFile('map')){

            $extension = Input::file('map')->getClientOriginalExtension();
            ### config.with, config.height
            $target_path        = Config::get('admin.contactus.target_path');
            $configWidth        = Config::get('admin.contactus.width');
            $configHeight       = Config::get('admin.contactus.height');   

            if($extension == 'pdf'){

                $file = Input::file('map'); 
                $filename = "map.".$extension;
                $file->move($target_path, $filename);

            }else{

                $filename = 'map.' . $extension;
                $img = Image::make(Input::file('map')->getRealPath())
                    ->resize($configWidth, $configHeight, true)
                    ->save($target_path . $filename);

            }

            $rec = Contactus::find($id);              
            $rec->update(array('map'=>$filename));

        }

        ##
        return Redirect::route('admin.contactus.show', $id); 
    }

}