<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use DB;

class SlidersController extends \BaseController {
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     * Make show.blade.php as crate
     */
    public function index()
    {

        $datas = Slider::select()->get();

        return \View::make('admin.slider.index')
            ->with('datas', $datas);

    }


    public function show($id)
    {

        $data = Slider::where('id', $id)->get();

        return \View::make('admin.slider.show')
            ->with('data', $data[0]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return \View::make('admin.slider.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        $validation = Validator::make(Input::all(), Slider::$rules_add);
        if($validation->fails())
        {
            return Redirect::route('admin.slider.create')
                ->withErrors($validation)
                ->withInput();
        }
        $exceptFields = array('width_image', 'height_image', 'width_thumb', 'height_thumb', 'img', 'x1', 'x2', 'y1', 'y2', 'w', 'h');
        $rec = Slider::create(Input::except($exceptFields));
        $lastInsertedId =  $rec->id;

        if(Input::has('image')){

            ### config.with, config.height
            $temp_dir           = Config::get('admin.upload.temp_dir');
            $target_path        = Config::get('admin.slider.target_path');
            // $configWidth        = Config::get('admin.slider.width');
            // $configHeight       = Config::get('admin.slider.height');
            // $configWidthThumb   = Config::get('admin.slider.width_thumb');
            // $configHeightThumb  = Config::get('admin.slider.height_thumb');
            
            ## Asssing file name
            $image = Input::get('image');
            $image_thumb = Input::get('image_thumb');

            ## Save image as target
            $img = Image::make($temp_dir . $image)
              // ->resize($configWidth, $configHeight, true)
              ->save($target_path . $image);
            
            ## Save image thumb as target
            $img_thumb = Image::make($temp_dir . $image_thumb)
                // ->resize($configWidthThumb, $configHeightThumb, true)
                ->save($target_path . $image_thumb);

            ## Update Image Name  
            $data = Slider::find($lastInsertedId);              
            $data->image         = $image;
            $data->image_thumb   = $image_thumb;
            $data->save();

        }
        return Redirect::route('admin.slider.index');
    }





    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $data = Slider::where('id', '=', $id)->get();

        if (is_null($data[0]))
        {
            return Redirect::back()->withInput();
        }

        return \View::make('admin.slider.edit')->with('data', $data[0]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
  
        $validation = Validator::make(Input::all(), Slider::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.slider.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('image', 'image_thumb', 'width_image', 'height_image', 'width_thumb', 'height_thumb', 'x1', 'x2', 'y1', 'y2', 'w', 'h');
        $data = Slider::find($id);
        $data->update(Input::except($exceptFields));

        if(Input::hasFile('img')){

            ### config.with, config.height
            $lastInsertedId = $data->id;
            $temp_dir           = Config::get('admin.upload.temp_dir');
            $target_path         = Config::get('admin.slider.target_path');
            // $configWidth        = Config::get('admin.slider.width');
            // $configHeight       = Config::get('admin.slider.height');
            // $configWidthThumb   = Config::get('admin.slider.width_thumb');
            // $configHeightThumb  = Config::get('admin.slider.height_thumb');
            
            ## Asssing file name
            $image = Input::get('image');
            $image_thumb = Input::get('image_thumb');

            ## Save image as target
            $img = Image::make($temp_dir . $image);
            $img->save($target_path . $image);
            
            ## Save image thumb as target
            $img_thumb = Image::make($temp_dir . $image_thumb);
            $img_thumb->save($target_path . $image_thumb);

            ## Update Image Name  
            $data = Slider::find($lastInsertedId);              
            $data->image         = $image;
            $data->image_thumb   = $image_thumb;
            $data->save();

        }

        return Redirect::route('admin.slider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = Slider::find($id);
        File::delete(array(
              Config::get('admin.slider.target_path').$data->image, 
              Config::get('admin.slider.target_path').$data->image_thumb
        ));

        Slider::find($id)->delete();

        return Redirect::back();
    }



}