<?php

namespace admin;
use Sentry;
use Session;
use Exception;
use Validator;
use Input;
use Auth;
use Redirect;
use Datatable;
use Paginate;
use Image;
use Config;
use File;
use Str;

class WhatsController extends \BaseController {
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = What::find(1);        
        return \View::make('admin.what.index')->with('data', $data);
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data = What::find($id);

        if (is_null($data))
        {
            return Redirect::route('admin.what.index');
        }

        return \View::make('admin.what.edit')->with('data', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {

        $validation = Validator::make(Input::all(), What::$rules_update);
        if($validation->fails())
        {
            Redirect::route('admin.what.edit', $id)
                ->withErrors($validation)
                ->withInput();
        }

        $exceptFields = array('image', 'image_thumb', 'width_image', 'height_image', 'width_thumb', 'height_thumb', 'x1', 'x2', 'y1', 'y2', 'w', 'h');

        $rec = What::find($id);
        $rec->update(Input::except($exceptFields));
        $lastInsertedId =  $id;

        # Success

        if(Input::hasFile('img')){

            ### config.with, config.height
            $temp_dir           = Config::get('admin.upload.temp_dir');
            $target_path        = Config::get('admin.what_image.target_path');

            ## Asssing file name
            $image = Input::get('image');
            $image_thumb = Input::get('image_thumb');

            ## Save image as target
            $img = Image::make($temp_dir . $image);
            $img->save($target_path . $image);

            ## Save image thumb as target
            $img = Image::make($temp_dir . $image_thumb);
            $img->save($target_path . $image_thumb);

            ## Update Image Name  
            $data = What::find($lastInsertedId);              
            $data->image  = $image;
            $data->image_thumb  = $image_thumb;
            $data->save();

        }
        return Redirect::route('admin.what.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = What::find($id);
        @File::delete(array(
            Config::get('admin.what_image.target_path').$data->image,
        ));

        return Redirect::route('admin.what.index');
    }

}