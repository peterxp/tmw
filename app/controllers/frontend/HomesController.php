<?php

namespace frontend;
use Session;
use Exception;
use Validator;
use Input;
use Redirect;
use Datatable;
use Paginate;
use ProductCategory;
use DB;
use Response;
use File;
use Request;
use Str;


class HomesController extends \BaseController {

    public function __construct(){
        $this->project_category = $this->projectCategory();
        // $this->link_banner = $this->linkBanner();
        $this->lang = Request::segment(1);
        Session::put('lang', $this->lang);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $address['address'] = $this->getAddress();

        $data = array(
            "slider"            => $this->slider(),
            "whatWeDo"          => $this->whatWeDo(),
            "recent_project"    => $this->recentProject(0, 6),
            "project_category"  => $this->projectCategoryMain(),
            "sidebar_activity"  => $this->activitySideBar(0, 3),
            "links"             => $this->linkBanner()
        );

        return \View::make('frontend.index')
            ->with('data', $data)
            ->with('footer', $address);
    }

    public function services(){

        $address['address'] = $this->getAddress();

        $data = array(
            "slider"            => $this->slider(),
            "project_category"  => $this->projectCategoryMain(),
            "recent_project"    => $this->recentProject(0, 5),
            "services"          => $this->getJsonTree(),
            "links"             => $this->linkBanner()
        );
        
        return \View::make('frontend.services')
            ->with('data', $data)
            ->with('categories', $this->getJsonTree())
            ->with('footer', $address);
    }


    public function portfolio()
    {

        $address['address'] = $this->getAddress();

        $ex = $this->recentProject(0,5);
        $arrExpProjectId = array();
        foreach ($ex as $k => $v) {
            $arrExpProjectId[] = $v->id;
        }

        $projects = DB::table('projects')
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->whereNotIn('id', $arrExpProjectId)
            ->paginate(12);

        $data = array(
            "slider"            => $this->slider(),
            "project_category"  => $this->projectCategoryMain(),
            "projects"          => $projects,
            "recent_project"    => $this->recentProject(0, 5),
            "links"             => $this->linkBanner()
        );
        
        return \View::make('frontend.portfolio')
            ->with('data', $data)
            ->with('categories', $this->getJsonTree())
            ->with('footer', $address);
    }

    public function projectByCategoryId($category_id = null)
    {

        $category_id = (!empty($category_id))? $category_id : 0;

        $ex = $this->recentProject(0, 5);
        $arrExpProjectId = array();
        foreach ($ex as $k => $v) {
            $arrExpProjectId[] = $v->id;
        }


        $projects = DB::table('projects')
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->whereNotIn('id', $arrExpProjectId)
            ->paginate(12);


        $address['address'] = $this->getAddress();
        
        $data = array(
            "projects"           => $projects,
            "slider"            => $this->slider(),
            "project_category"  => $this->projectCategoryMain(),
            "recent_project"    => $this->recentProject(0, 5),
            "links"             => $this->linkBanner()
        );

        return \View::make('frontend.portfolio')
            ->with('data', $data)
            ->with('categories', $this->getJsonTree())
            ->with('footer', $address);

    }

    public function activity()
    {

        $address['address'] = $this->getAddress();

        $activities = DB::table('activities')
            ->orderBy('id')
            ->paginate(10);

        $data = array(
            "slider"            => $this->slider(),
            "project_category"  => $this->projectCategoryMain(),
            "activities"        => $activities,
            "recent_project"    => $this->recentProject(0, 5),
            "links"             => $this->linkBanner()
        );
        
        return \View::make('frontend.activity')
            ->with('data', $data)
            ->with('footer', $address);
    }




    public function activtyDetail($activity_id){
        
        $activities = DB::table('activities')
            ->where('id', $activity_id)
            ->get();

        $activity_gallery = DB::table('activitygalleries')
            ->where('activitygalleries_id', $activity_id)
            ->get();

        $data = array(
            "activities"        => $activities,
            "activity_gallery"  => $activity_gallery,
            "links"             => $this->linkBanner()
        );

        return \View::make('frontend.activity_detail')->with('data', $data);
    }

    public function projectDetail($project_id){

        $projects = DB::table('projects')->select(['projects.*','pc.title as category_title', 'pc.title_en as category_title_en'])
            ->Join('projectcategories as pc', 'projects.projectcategories_id', '=', 'pc.id')
            ->where('projects.id', $project_id)
            ->get();

        $project_gallery = DB::table('projectgalleries')
            ->where('project_id', $project_id)
            ->get();

        $data = array(
            "projects"          => $projects,
            "project_gallery"   => $project_gallery
        );

        return \View::make('frontend.project_detail')->with('data', $data);
    }

    public function ourCustomers(){

        $address['address'] = $this->getAddress();

        $customers = DB::table('customers')->paginate(15);

        $data = array(
            "slider"            => $this->slider(),
            "project_category"  => $this->projectCategoryMain(),
            "customers"         => $customers,
            "recent_project"    => $this->recentProject(0, 5),
            "links"             => $this->linkBanner()
        );
        
        return \View::make('frontend.our_customers')
            ->with('data', $data)
            ->with('footer', $address);
    }

    public function aboutus(){

        $address['address'] = $this->getAddress();

        $aboutus = DB::table('aboutus')
            ->where('id', 1)
            ->get();

        $data = array(
            "slider"            => $this->slider(),
            "project_category"  => $this->projectCategoryMain(),
            "aboutus"           => $aboutus,
            "recent_project"    => $this->recentProject(0, 5),
            "links"             => $this->linkBanner()
        );
        
        return \View::make('frontend.aboutus')
            ->with('data', $data)
            ->with('footer', $address);
    }

    public function jobVacancy()
    {
        $address['address'] = $this->getAddress();

        $jobs = DB::table('jobs')
            ->orderBy('id', 'DESC')
            ->get();

        $data = array(
            "slider"            => $this->slider(),
            "project_category"  => $this->projectCategoryMain(),
            "jobs"              => $jobs,
            "recent_project"    => $this->recentProject(0, 5),
            "links"             => $this->linkBanner()
        );

        return \View::make('frontend.job_vacancy')
            ->with('data', $data)
            ->with('footer', $address);
    }

    public function contactus(){

        $address['address'] = $this->getAddress();

        $contactus = DB::table('contactuses')
            ->where('id', 1)
            ->get();

        $data = array(
            "slider"            => $this->slider(),
            "project_category"  => $this->projectCategoryMain(),
            "recent_project"    => $this->recentProject(0, 5),
            "contactus"         => $contactus,
            "links"             => $this->linkBanner()
        );
        
        return \View::make('frontend.contactus')
            ->with('data', $data)
            ->with('footer', $address);
    }


    public function search(){

        return \View::make('frontend.search');

    }

    public function getAddress()
    {
        return $contactus = DB::table('contactuses')
            ->select(['*'])
            ->where('id', 1)
            ->get();
    }

    public function projectCategoryMain(){

        return DB::table('projectcategories')->select('id', 'title', 'title_en')
            ->where('parent_id', 0)
            ->orderBy('title', 'ASC')
            ->orderBy('title_en', 'ASC')
            ->get();
    }

    public function projectCategory(){

        return DB::table('projectcategories')->select('id', 'title', 'title_en')
            ->orderBy('title', 'ASC')
            ->orderBy('title_en', 'ASC')
            ->get();
    }

    public function slider(){

        return DB::table('sliders')->select()
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function recentProject($start, $limit){

        return DB::table('projects')->select()
            ->skip($start)
            ->take($limit)
            ->orderBy('updated_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function activitySideBar($start, $limit){
               
        return DB::table('activities')
            ->skip($start)
            ->take($limit)
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function whatWeDo(){
        return DB::table('whats')->where('id', 1)->get();
    }

    public function linkBanner(){
        return DB::table('links')
            ->orderBy('id')
            ->get();
    }



    /**
    * Create Tree have 2 function createBranch(), createTree
    **/
    function createBranch(&$parents, $children) 
    {
        $tree = array();
        foreach ($children as $child) 
        {
            if (isset($parents[$child['id']])) 
            {
                $child['children'] = $this->createBranch($parents, $parents[$child['id']]);
            }
            $tree[] = $child;
        } 
        return $tree;
    }

    function createTree($flat, $root = 0) 
    {
        $parents = array();
        foreach ($flat as $a) 
        {
            $parents[$a['parent_id']][] = $a;
        }
        return $this->createBranch($parents, $parents[$root]);
    }


    public function getJsonTree(){

        $cat = DB::table('projectcategories')->select(['*'])
            ->orderBy('parent_id', 'ASC')
            ->orderBy('title_en', 'ASC')
            ->get();

        # Prepare Array
        $arr = array();
        foreach ($cat as $key => $value) {
            $arr[$key] = array(
                'id'        =>$value->id,
                'parent_id' =>$value->parent_id,
                'title'     =>$value->title,
                'title_en'  =>$value->title_en,
                'description'       =>$value->description,
                'description_en'    =>$value->description_en,
                'image'             =>$value->image
            );
         }

        return $this->createTree($arr);

    }

}