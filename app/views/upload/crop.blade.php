<!DOCTYPE html>
<html>
<head>
	<title>Upload Image</title>

	<!-- main jquery -->
    {{HTML::script('packages/jquery/jquery.min.js')}}
    {{HTML::script('packages/bootstrap/js/bootstrap.min.js')}}

  {{HTML::script('packages/AjaxFileUpload/jquery.ajaxfileupload.js')}}

  <!-- jCrop -->
  {{HTML::style('packages/jcrop/css/jquery.Jcrop.min.css')}}
  {{HTML::script('packages/jcrop/js/jquery.Jcrop.min.js')}}



<style type="text/css">
div#image_area{
  margin:0px;
  padding:0px;
  border:1px;
  display: block;
}

</style></head>
<body>

{{Form::open(array('url'=>'upload_save', 'method'=>'POST', 'files'=>true))}}	

  {{Form::label('image', 'Image: ')}}
  {{Form::file('image')}}

  <br/>

  <label>X1 <input type="text" size="4" id="x1" name="x1" /></label>
  <label>Y1 <input type="text" size="4" id="y1" name="y1" /></label>
  <label>X2 <input type="text" size="4" id="x2" name="x2" /></label>
  <label>Y2 <input type="text" size="4" id="y2" name="y2" /></label>
  <label>W <input type="text" size="4" id="w" name="w" /></label>
  <label>H <input type="text" size="4" id="h" name="h" /></label>

  Image Name<input type="text" name="image" id="image">
  Image Name<input type="text" name="image_thumb" id="image_thumb">


  <br/><br/>



  <div id="image_area" style="border:1px solid #00f"></div>

  <div id="image_thumb_area" style="border:1px solid #00f">
    <img id="image_thumb" src="">
  </div>

  <input type="button" onclick="crop_image();" name="crop_thumb" value="Crop Image">

  {{Form::submit('Submit', array('id'=>'submit', 'class'=>'btn', 'onClick'=>'return checkCrop();'))}}

{{Form::close()}}


<script type="text/javascript">

	function checkCrop(){
	  if( $('#x1').val() =='' || $('#y1').val() ){
	    alert('Please crop image befor submit data');
	    return false;
	  }
	}



	$(document).ready(function(){

	  $('input#image').ajaxfileupload(
	    {
	      'action': '/upload_crop_temp',
	      'params': {
	      'extra': 'info'
	    },
	    'onStart': function() 
	    {
	      //if(weWantedTo) return false; // cancels upload
	      $('#image_area').html('');
	      // $('img#preview').attr('src', '');
	    },
	    'onComplete': function(resp) 
	    {
		    //do anything here as you want
		    console.log(resp.img)
		    //alert(JSON.stringify(response));
		    var image_path = '/upload/temp_dir/';
		    
		    $('#image_area').append('<img id="imagex" src="' + image_path + resp.image + '"/>');
		    $('input#image_name').attr('value', resp.image);
		    // $('img#preview').attr('src', resp.img);

		    $('img#imagex').Jcrop({
		    	boxWidth:   800, 
		        boxHeight:  600,
		        onSelect:   showCoords,
		        bgColor:    'black',
		        bgOpacity:  .7,
		        setSelect:  [ 0, 0, 120, 80 ],
		        aspectRatio: 4 / 3
		    });

	    }

	  	});
	});


	function crop_image()
	{
	  
	  	var image_path = '/upload/temp_dir/';

		$.ajax({
		url: '/upload_crop',
		type: 'POST',
		data: { 
		    x1: $('#x1').val(), 
		    y1: $('#y1').val(),
		    x2: $('#x2').val(),
		    y2: $('#y2').val(),
		    w: $('#w').val(),
		    h: $('#h').val(),
		    image_name: $('#image_name').val()
		},
		success: function(resp) {
		    //called when successful

		    console.log(resp.image_thumb);

			$('#image_thumb_area img#image_thumb').attr('src', image_path + resp.image_thumb);
		    $('input#image_thumb').attr('value', resp.image_thumb); 
		    console.log('ok');
		},
		error: function(e) {
		    //called when there is an error
			console.log(e);
		}

	});
	}

  	function showCoords(c)
  	{
    jQuery('#x1').val(c.x);
    jQuery('#y1').val(c.y);
    jQuery('#x2').val(c.x2);
    jQuery('#y2').val(c.y2);
    jQuery('#w').val(c.w);
    jQuery('#h').val(c.h);
  };


</script>




</body>
</html>