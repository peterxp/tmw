@extends('admin._layouts.master')

@section('content')

  <div class="row">
    <div class="col-lg-12">
      <h1>Dashboard <small></small></h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
      </ol>
      <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Welcome to TMW ADMIN PANEL, Feel free to use it.
      </div>
    </div>
  </div><!-- /.row -->


  <div class="row">
 
    <div class="col-lg-3">
      <div class="panel panel-success">
       
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-6">
              <i class="fa fa-tasks fa-5x"></i>
            </div>
            <div class="col-xs-6 text-right">
              <p class="announcement-heading">{{$data['project_category_count']}}</p>
              <p class="announcement-text">Our Service</p>
            </div>
          </div>
        </div>

        <a href="{{URL::to('admin/projectcategory')}}">
          <div class="panel-footer announcement-bottom">
            <div class="row">
              <div class="col-xs-6">
                View All
              </div>
              <div class="col-xs-6 text-right">
                <i class="fa fa-arrow-circle-right"></i>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>


    <div class="col-lg-3">
      <div class="panel panel-info">
       
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-6">
              <i class="fa fa-tasks fa-5x"></i>
            </div>
            <div class="col-xs-6 text-right">
              <p class="announcement-heading">{{$data['project_count']}}</p>
              <p class="announcement-text">Project</p>
            </div>
          </div>
        </div>

        <a href="{{URL::to('admin/project')}}">
          <div class="panel-footer announcement-bottom">
            <div class="row">
              <div class="col-xs-6">
                View All
              </div>
              <div class="col-xs-6 text-right">
                <i class="fa fa-arrow-circle-right"></i>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="panel panel-warning">
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-6">
              <i class="fa fa-check fa-5x"></i>
            </div>
            <div class="col-xs-6 text-right">
              <p class="announcement-heading">{{$data['activity_count']}}</p>
              <p class="announcement-text">Activity</p>
            </div>
          </div>
        </div>
        <a href="{{URL::to('admin/activity')}}">
          <div class="panel-footer announcement-bottom">
            <div class="row">
              <div class="col-xs-6">
                View All
              </div>
              <div class="col-xs-6 text-right">
                <i class="fa fa-arrow-circle-right"></i>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="col-lg-3">
      <div class="panel panel-danger">
       
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-6">
              <i class="fa fa-tasks fa-5x"></i>
            </div>
            <div class="col-xs-6 text-right">
              <p class="announcement-heading">{{$data['ourcustomer_count']}}</p>
              <p class="announcement-text">Our Customer</p>
            </div>
          </div>
        </div>

        <a href="{{URL::to('admin/customer')}}">
          <div class="panel-footer announcement-bottom">
            <div class="row">
              <div class="col-xs-6">
                View All
              </div>
              <div class="col-xs-6 text-right">
                <i class="fa fa-arrow-circle-right"></i>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>


    <div class="col-lg-3">
      <div class="panel panel-info">
       
        <div class="panel-heading">
          <div class="row">
            <div class="col-xs-6">
              <i class="fa fa-tasks fa-5x"></i>
            </div>
            <div class="col-xs-6 text-right">
              <p class="announcement-heading">{{$data['job_vacancy_count']}}</p>
              <p class="announcement-text">Job Vacancy</p>
            </div>
          </div>
        </div>

        <a href="{{URL::to('admin/job')}}">
          <div class="panel-footer announcement-bottom">
            <div class="row">
              <div class="col-xs-6">
                View All
              </div>
              <div class="col-xs-6 text-right">
                <i class="fa fa-arrow-circle-right"></i>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>

    </div><!-- /.row -->
@stop