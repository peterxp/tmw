<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CERNIC ADMIN PANEL</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
    {{ HTML::style('css/admin/font-awesome/css/font-awesome.min.css') }}

    <!-- Add custom CSS here -->
    {{ HTML::style('css/admin/sb-admin.css') }}
    {{ HTML::style('css/admin/admin.css') }}

    <!-- Datatables -->
    {{ HTML::style('packages/datatables/media/css/DT_bootstrap.css') }}


    <!-- JavaScript -->
    {{HTML::script('packages/jquery/jquery.min.js')}}
    {{HTML::script('packages/bootstrap/js/bootstrap.min.js')}}

    <!-- Page Specific Plugins -->
    <!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->

    <!-- DataTables -->
    {{ HTML::script('packages/datatables/media/js/jquery.dataTables.min.js') }}
    {{ HTML::script('packages/datatables/media/js/DT_bootstrap.js') }}


    <!-- jQTree -->
    {{ HTML::style('packages/jqtree/jqtree.css') }}
    {{ HTML::script('packages/jqtree/tree.jquery.js') }}
    {{ HTML::script('packages/jquery-cookie/jquery.cookie.js') }}

  
  </head>

  <body>

    <div id="wrapper">
      <div id="page-wrapper">

          @yield('content')
  
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->

  </body>
</html>
