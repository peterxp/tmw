<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TMW ADMIN PANEL</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
    {{ HTML::style('css/admin/font-awesome/css/font-awesome.min.css') }}

    <!-- Add custom CSS here -->
    {{ HTML::style('css/admin/sb-admin.css') }}
    {{ HTML::style('css/admin/admin.css') }}

    <!-- Datatables -->
    {{ HTML::style('packages/datatables/media/css/DT_bootstrap.css') }}


    <!-- JavaScript -->
    {{HTML::script('packages/jquery/jquery.min.js')}}
    {{HTML::script('packages/bootstrap/js/bootstrap.min.js')}}

    <!-- Page Specific Plugins -->
    <!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->

    <!-- DataTables -->
    {{ HTML::script('packages/datatables/media/js/jquery.dataTables.min.js') }}
    {{ HTML::script('packages/datatables/media/js/DT_bootstrap.js') }}

    <!-- jQTree -->
    {{ HTML::style('packages/jqtree/jqtree.css') }}
    {{ HTML::script('packages/jqtree/tree.jquery.js') }}
    {{ HTML::script('packages/jquery-cookie/jquery_cookie.js') }}

    {{HTML::script('packages/AjaxFileUpload/jquery.ajaxfileupload.js')}}
  	<!-- jCrop -->
  	{{HTML::style('packages/jcrop/css/jquery.Jcrop.min.css')}}
  	{{HTML::script('packages/jcrop/js/jquery.Jcrop.min.js')}}

   	<!-- FancyBox -->
  	{{HTML::style('packages/fancybox/source/jquery.fancybox.css')}}
  	{{HTML::script('packages/fancybox/source/jquery.fancybox.pack.js')}}

    <!-- ckeditor -->
    {{HTML::script('packages/ckeditor/ckeditor.js')}}
    {{HTML::script('packages/ckeditor/adapters/jquery.js')}}

  </head>
  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="">
            <a class="navbar-brand" href="index.html">TMW ADMIN PANEL</a>
          </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->


          
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            
            <li class="@if(Request::segment(2) == 'dashboard') active @endif">
            	<a href="{{URL::to('admin/dashboard')}}">
              	<i class="fa fa-dashboard"></i> Dashboard</a>
            </li>

            <li class="@if(Request::segment(2) == 'slider') active @endif">
              <a href="{{URL::to('admin/slider')}}"><i class="fa fa-table"></i> Main Slide</a>
            </li>

            <li class="@if(Request::segment(2) == 'what') active @endif">
              <a href="{{URL::to('admin/what')}}"><i class="fa fa-table"></i> WHAT WE DO?</a>
            </li>

            <li class="@if(Request::segment(2) == 'projectcategory') active @endif">
              <a href="{{URL::to('admin/projectcategory')}}"><i class="fa fa-table"></i> Our Service/Project Cat</a>
            </li>

            <li class="@if((Request::segment(2) == 'project') || (Request::segment(2) == 'projectgallery')) active @endif">
            	<a href="{{URL::to('admin/project')}}"><i class="fa fa-table"></i> Project</a>
            </li>

            <li class="@if(Request::segment(2) == 'link') active @endif">
              <a href="{{URL::to('admin/link')}}"><i class="fa fa-table"></i> Links</a>
            </li>

            <li class="@if((Request::segment(2) == 'activity') || (Request::segment(2) == 'activitygallery')) active @endif">
              <a href="{{URL::to('admin/activity')}}"><i class="fa fa-table"></i> Activity</a>
            </li>


            <li class="@if(Request::segment(2) == 'customer') active @endif">
              <a href="{{URL::to('admin/customer')}}"><i class="fa fa-table"></i> Our Customers</a>
            </li>           

            <li class="@if(Request::segment(2) == 'aboutus') active @endif">
            	<a href="{{URL::to('admin/aboutus')}}">
            	<i class="fa fa-table"></i> About Us</a>
            </li> 

             <li class="@if(Request::segment(2) == 'job') active @endif">
              <a href="{{URL::to('admin/job')}}"><i class="fa fa-table"></i> Job Vacancy</a>
            </li>

            <li class="@if(Request::segment(2) == 'contactus') active @endif">
            	<a href="{{URL::to('admin/contactus/1')}}"><i class="fa fa-envelope"></i> Contactus</a>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
             {{Sentry::getUser()->first_name}}

              <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="{{URL::to('admin/user')}}"><i class="fa fa-user"></i> Profile</a></li>
               	<!--  <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li> -->
                <li class="divider"></li>
                <li><a href="{{URL::to('administrator/logout')}}"><i class="fa fa-power-off"></i> Log Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">

          @yield('content')
  
      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

  	<!-- Start FancyBox Global -->
  	<script type="text/javascript">

	  	$(document).ready(function() {
	    	$(".facnybox_single").fancybox({
	          	helpers: {
	              	title : {
	                  	type : 'float'
	              }
	          	}
	      	});
	    });

    </script>
    <!-- End FancyBox Global -->

  </body>
</html>
