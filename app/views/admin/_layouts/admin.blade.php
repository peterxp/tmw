<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Authentication</title>
  {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}

  {{ HTML::style('css/main.css') }}
</head>
<body>

      <!-- Fixed navbar -->
      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
              <li>{{ HTML::link('admin/logout', 'Logout') }}</li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>


    <div class="container">
        @yield('content')
    </div>
    
</body>
</html>