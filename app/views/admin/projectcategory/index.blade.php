@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Portfolio</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="projectcategory/create" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-plus-sign"></span> Add
                    </a>
                </div>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">
            <div class="col-lg-12">
                <hr class="thin" />
                <br/>
                <div class="col-sm-offset-1 col-sm-11">
                    <!-- Generate Tree Menu -->
                    <div id="tree1" data-url="{{$data->jsonURL}}"></div>
                </div>
     
            </div>
        </div>
        <!-- content body end -->


<script type="text/javascript">

$(function() {
    var $tree = $('#tree1');

    $tree.tree({
        autoOpen: 1,
        saveState: true,
        onCreateLi: function(node, $li) {
            // Append a link to the jqtree-element div.
            // The link has an url '#node-[id]' and a data property 'node-id'.
            $li.find('.jqtree-element').append(
                ' <a href="projectcategory/'+ node.id +'" class="edit" data-node-id="'+ node.id +'">View</a>' + 
                ' <a href="projectcategory/'+ node.id +'/edit" class="edit" data-node-id="'+ node.id +'">Edit</a>' +
                ' <form method="POST" action="projectcategory/'+ node.id +'" accept-charset="UTF-8" class="form-delete"><input name="_method" type="hidden" value="DELETE"><button onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};" class="btn-as-link"> Delete</button></form>'
                
            );

            //$li.find('div.jqtree-element').addClass('jqtree-toggler');
        }
    });

    // Handle a click on the edit link
    $tree.on(
        'click', '.edit',
        function(e, $li) {
            // Get the id from the 'node-id' data property
            var node_id = $(e.target).data('node-id');
            console.log('node_id : ' + node_id);

            // Get the node from the tree
            var node = $tree.tree('getNodeById', node_id);

            if (node) {
                // Display the node name
               // alert(node.name);

            //$li.find('div.jqtree-element').addClass('jqtree-toggler');

            }
        }
    );
});

</script>

@stop