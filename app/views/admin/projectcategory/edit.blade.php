@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Portfolio</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="/admin/projectcategory" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a>
                 </div>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>

             {{ Form::open(array('method' => 'PATCH', 'files'=>TRUE, 'id'=>'myForm', 'class'=>'form-horizontal', 'role'=>'form', 'route' => array('admin.projectcategory.update', $data->id))) }}

                 <div class="form-group">
                    <label for="category_name" class="col-sm-2 control-label">Main Category</label>
                    <div class="col-sm-8">
                      {{Form::select('parent_id', $arrSelectBox, $data->parent_id, array('id'=>'parent_id', 'class'=>'form-control'));}}
                    </div>
                  </div>
                  
                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-8">
                      {{ Form::text('title', $data->title, array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title [EN]</label>
                    <div class="col-sm-8">
                      {{ Form::text('title_en', $data->title_en, array('class'=>'form-control')) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('description', $data->description, array('class'=>'form-control', 'size' => '70x3')) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description[EN]</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('description_en', $data->description_en, array('class'=>'form-control', 'size' => '70x3')) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="link_banner" class="col-sm-2 control-label">Image</label>
                    <div class="col-sm-8">
						          @if($data->image != "")
                    		{{HTML::image('/upload/images/projectcategory_icon/'.$data->image, null, null)}}
                    	@else
                    		-
                    	@endif

                   		<br/><br/>
                    	<span class="btn btn-info btn-file">
                       		Browse File.. {{Form::file('image', array('type'=>'file', 'name'=>'image', 'id'=>'image'))}}
                      </span>
                      <span class="suggest_image_size">
                        *Image size 140x140 Pixels
                      </span>                      
                    </div>
                  </div>    

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
          
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-floppy-disk">
                                    Save
                                </span>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <span class="glyphicon glyphicon-floppy-remove">
                                    Reset
                                </span>
                            </button>
                   
                    </div>
                  </div>


            {{ Form::close() }}


                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    @if ($errors->any())
                        <ul>
                            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                        </ul>
                    @endif
                </div>
                </div>


          </div>
        </div>
        <!-- content body end -->

@stop