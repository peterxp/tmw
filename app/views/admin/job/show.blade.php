@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Job Vacancy</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">
                    <a href="/admin/job" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a>
                 </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>


              <div class="panel panel-info">
              <div class="panel-heading">Detail</div>
                <table class="table table-hover">
                  <tbody>

                    <tr>
                      <td class="topic_left width_150">Topic</td>
                      <td>{{$job->topic}}</td>
                    </tr>
                    <tr>
                      <td class="topic_left">Topic [EN]</td>
                      <td>{{$job->topic_en}}</td>
                    </tr>
                    
                    <tr>
                      <td class="topic_left">Detail</td>
                      <td>{{$job->detail}}</td>
                    </tr>
                    <tr>
                      <td class="topic_left">Detail [EN]</td>
                      <td>{{$job->detail_en}}</td>
                    </tr>

                    <tr>
                    	<td></td>
                    	<td>
                    		{{ HTML::link_nested('admin.job.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($job->id)) }}
                    	</td>

                    </tr>
                  </tbody>
                </table>
              </div>

          </div>
        </div>
        <!-- content body end -->

@stop