@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Job Vacancy</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">
                    <a href="job/create" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-plus-sign"></span> Add
                    </a>
                </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">
          <div class="col-lg-12">
                    <hr class="thin" />

                    <table class="table table-striped table-bordered table-hover" id="myDataTable">
                        <thead>
                            <tr>
                                <th class="running_number">No.</th>
                                <th>Topic</th>
                                <th>Topic(EN)</th>
                                <th class="action center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($jobs as $job): ?>
                            <tr class="odd gradeX">
                                <td class="running_number">{{$job->id}}</td>
                                <td>{{$job->topic}}</td>
                                <td>{{$job->topic_en}}</td>
                                <td class="action center">

                                    {{ HTML::link_nested('admin.job.show', 'View', array('class'=>'btn btn-sm btn-info'), '', '<span class="glyphicon glyphicon-ok-sign"></span> ', array($job->id)) }}

                                    {{ HTML::link_nested('admin.job.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($job->id)) }}

                                    {{ HTML::delete_button('admin.job.destroy', $job->id, 'Delete', array('class'=>'btn btn-sm btn-danger'), '', '<span class="glyphicon glyphicon-trash"></span> ') }}

                                </td>
                            </tr>
                            <?php endforeach; ?>
            
                        </tbody>
                    </table>
     
          </div>
        </div>
        <!-- content body end -->


<script type="text/javascript">

/* Table initialisation */
$(document).ready(function() {
  $('#myDataTable').dataTable( {
    "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
    "sPaginationType": "bootstrap",
    "oLanguage": {
      "sLengthMenu": "_MENU_ records per page"
    },
    "aLengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
    "aoColumnDefs": 
        [ 
            { "bSortable": false, "aTargets": [ 3 ] }
        ],

    "aaSorting": [[ 0, "desc" ]]
  });
});

</script>

@stop