@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Job Vacancy</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">
                    <a href="/admin/job" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a>
                 </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>

            {{ Form::open(array('route' => 'admin.job.store', 'method' => 'POST', 'id'=>'myForm', 'class'=>'form-horizontal', 'role'=>'form')) }}

                 <div class="form-group">
                    <label for="topic" class="col-sm-2 control-label">Topic</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('topic','', array('class'=>'form-control','rows'=>'2')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="question_en" class="col-sm-2 control-label">Topic [EN]</label>
                    <div class="col-sm-8">
                       {{ Form::textarea('topic_en','', array('class'=>'form-control','rows'=>'2')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="detail" class="col-sm-2 control-label">Detail</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('detail','', array('id'=>'detail', 'class'=>'form-control', 'rows'=>'15')) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="detail_en" class="col-sm-2 control-label">Detail [EN]</label>
                    <div class="col-sm-8">
                       {{ Form::textarea('detail_en','', array('class'=>'form-control', 'rows'=>'15')) }}
                    </div>
                  </div>                 

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
          
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-floppy-disk">
                                    Save
                                </span>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <span class="glyphicon glyphicon-floppy-remove">
                                    Reset
                                </span>
                            </button>
                   
                    </div>
                  </div>


            {{ Form::close() }}

                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    @if ($errors->any())
                        <ul>
                            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                        </ul>
                    @endif
                </div>
                </div>


          </div>
        </div>
        <!-- content body end -->

<script type="text/javascript">

CKEDITOR.replace( 'detail', {
  toolbar: [
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript' ] },
    { name: 'paragraph', groups: [ 'list'], items: [ 'NumberedList', 'BulletedList', 'Blockquote' ] }
  ]
});

CKEDITOR.replace( 'detail_en', {
  toolbar: [
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript' ] },
    { name: 'paragraph', groups: [ 'list'], items: [ 'NumberedList', 'BulletedList', 'Blockquote' ] }
  ]
});

</script>

@stop