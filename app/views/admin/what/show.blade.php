@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Company Profile</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">
<!--                     <a href="/admin/productcategory" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a> -->
                 </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>


              <div class="panel panel-info">
              <div class="panel-heading">Detail</div>
                <table class="table table-hover">
                  <tbody>

                    <tr>
                      <td class="topic_left">Message</td>
                      <td>{{$data->msg}}</td>
                    </tr>

                    <tr>
                      <td class="topic_left">Message [EN]</td>
                      <td>{{$data->msg_en}}</td>
                    </tr>
                      <tr>
                      <td class="topic_left">created_at</td>
                      <td>{{$data->created_at}}</td>
                    </tr>

                     <tr>
                      <td class="topic_left">updated_at</td>
                      <td>{{$data->updated_at}}</td>
                    </tr>

                    <tr>
                    <td class="topic_left">sdfsdfsdfsd</td>
                    <td>
						{{ HTML::link_nested('admin.product.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($data->id)) }}
                    </td>
                    </tr>

                  </tbody>
                </table>
              </div>

          </div>
        </div>
        <!-- content body end -->

@stop