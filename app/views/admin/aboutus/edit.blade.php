@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> About Us</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">
                    <a href="/admin/aboutus" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a>
                 </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>

            {{ Form::open(array('method' => 'PATCH', 'files'=>TRUE, 'id'=>'myForm', 'class'=>'form-horizontal', 'role'=>'form', 'route' => array('admin.aboutus.update', $data->id))) }}

                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-8">
                      {{ Form::text('title', $data->title_en, array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title_en" class="col-sm-2 control-label">Title [EN]</label>
                    <div class="col-sm-8">
                      {{ Form::text('title_en', $data->title_en, array('class'=>'form-control')) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="link_banner" class="col-sm-2 control-label">Top Image</label>
                    <div class="col-sm-8">
                        @if($data->image != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image, null, null)}}
                        @else
                            -
                        @endif
                        <br/><br/>
                        <span class="btn btn-info btn-file">
                            Browse File.. {{Form::file('image', array('type'=>'file', 'name'=>'image', 'id'=>'image'))}}
                        </span>
                        <span class="suggest_image_size">
                          *Image size 665x382 Pixels
                        </span>

                    </div>
                  </div> 

                 <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('description', $data->description, array('class'=>'form-control', 'rows'=>'4')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="description_en" class="col-sm-2 control-label">Description [EN]</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('description_en', $data->description_en, array('class'=>'form-control', 'rows'=>'4')) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="image1" class="col-sm-2 control-label">Small Image#1</label>
                    <div class="col-sm-8">
                        @if($data->image1 != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image1, null, null)}}
                        @else
                            -
                        @endif
                        <br/><br/>
                        <span class="btn btn-info btn-file">
                            Browse File.. {{Form::file('image1', array('type'=>'file', 'name'=>'image1', 'id'=>'image1'))}}
                        </span>
                        <span class="suggest_image_size">
                          *Image size 150x100 Pixels
                        </span>                        
                    </div>
                  </div> 

                  <div class="form-group">
                    <label for="image2" class="col-sm-2 control-label">Small Image#2</label>
                    <div class="col-sm-8">
                        @if($data->image2 != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image2, null, null)}}
                        @else
                            -
                        @endif
                        <br/><br/>
                        <span class="btn btn-info btn-file">
                            Browse File.. {{Form::file('image2', array('type'=>'file', 'name'=>'image2', 'id'=>'image2'))}}
                        </span>
                        <span class="suggest_image_size">
                          *Image size 150x100 Pixels
                        </span>                         
                    </div>
                  </div> 

                  <div class="form-group">
                    <label for="image3" class="col-sm-2 control-label">Small Image#3</label>
                    <div class="col-sm-8">
                        @if($data->image3 != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image3, null, null)}}
                        @else
                            -
                        @endif
                        <br/><br/>
                        <span class="btn btn-info btn-file">
                            Browse File.. {{Form::file('image3', array('type'=>'file', 'name'=>'image3', 'id'=>'image3'))}}
                        </span>
                        <span class="suggest_image_size">
                          *Image size 150x100 Pixels
                        </span>                         
                    </div>
                  </div> 

                  <div class="form-group">
                    <label for="image4" class="col-sm-2 control-label">Small Image#4</label>
                    <div class="col-sm-8">
                        @if($data->image4 != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image4, null, null)}}
                        @else
                            -
                        @endif
                        <br/><br/>
                        <span class="btn btn-info btn-file">
                            Browse File.. {{Form::file('image4', array('type'=>'file', 'name'=>'image4', 'id'=>'image4'))}}
                        </span>
                        <span class="suggest_image_size">
                          *Image size 150x100 Pixels
                        </span>                         
                    </div>
                  </div> 

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
          
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-floppy-disk">
                                    Save
                                </span>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <span class="glyphicon glyphicon-floppy-remove">
                                    Reset
                                </span>
                            </button>
                   
                    </div>
                  </div>


            {{ Form::close() }}

                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    @if ($errors->any())
                        <ul>
                            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                        </ul>
                    @endif
                </div>
                </div>


          </div>
        </div>
        <!-- content body end -->

@stop