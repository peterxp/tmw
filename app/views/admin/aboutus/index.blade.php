@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> About Us</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">

                 </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>


              <div class="panel panel-info">
              <div class="panel-heading">Detail</div>
                <table class="table table-hover">
                  <tbody>

                    <tr>
                      <td class="topic_left" width="150">Title</td>
                      <td>{{$data->title}}</td>
                    </tr>

                    <tr>
                      <td class="topic_left">Title [EN]</td>
                      <td>{{$data->title_en}}</td>
                    </tr>

                    <tr>
                      <td class="topic_left">Top Image</td>
                      <td>
                          @if($data->image != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image, null, null)}}
                          @else
                            -
                          @endif
                      </td>
                    </tr>

                    <tr>
                      <td class="topic_left">Description</td>
                      <td>{{$data->description}}</td>
                    </tr>

                    <tr>
                      <td class="topic_left">Description [EN]</td>
                      <td>{{$data->description_en}}</td>
                    </tr>

                    <tr>
                      <td class="topic_left">Small Image#1</td>
                      <td>
                          @if($data->image1 != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image1, null, null)}}
                          @else
                            -
                          @endif
                      </td>
                    </tr>

                    <tr>
                      <td class="topic_left">Small Image#2</td>
                      <td>
                          @if($data->image2 != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image2, null, null)}}
                          @else
                            -
                          @endif
                      </td>
                    </tr>

                    <tr>
                      <td class="topic_left">Small Image#3</td>
                      <td>
                          @if($data->image3 != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image3, null, null)}}
                          @else
                            -
                          @endif
                      </td>
                    </tr>

                    <tr>
                      <td class="topic_left">Small Image#4</td>
                      <td>
                          @if($data->image3 != "")
                            {{HTML::image('/upload/images/aboutus/'.$data->image4, null, null)}}
                          @else
                            -
                          @endif
                      </td>
                    </tr>

                    <tr>
                      <td class="topic_left">Created Date</td>
                      <td>{{$data->created_at}}</td>
                    </tr>

                    <tr>
                      <td class="topic_left">Updated Date</td>
                      <td>{{$data->updated_at}}</td>
                    </tr>

                    <tr>
                    	<td>&nbsp;</td>
                    	<td>
                    		{{ HTML::link_nested('admin.aboutus.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($data->id)) }}
                    	</td>
                    </tr>

                  </tbody>
                </table>
              </div>

          </div>
        </div>
        <!-- content body end -->

@stop