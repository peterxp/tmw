@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Links</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="link/create" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-plus-sign"></span> Add
                    </a>
                </div>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">
          <div class="col-lg-12">
                    <hr class="thin" />

                    <table class="table table-striped table-bordered table-hover" id="myDataTable">
                        <thead>
                            <tr>
                            	<th class="running_number width_150">ID</th>
                                <th>Title</th>
                                <th class="center width290">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data as $data): ?>
                            <tr class="odd gradeX">
                            	<td class="">{{$data->id}}</td>
                                <td>{{$data->title}}</td>
                                <td class="width290 center">

                                    {{ HTML::link_nested('admin.link.show', 'View', array('class'=>'btn btn-sm btn-primary'), '', '<span class="glyphicon glyphicon-ok-sign"></span> ', array($data->id)) }}

                                    {{ HTML::link_nested('admin.link.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($data->id)) }}

                                    {{ HTML::delete_button('admin.link.destroy', $data->id, 'Delete', array('class'=>'btn btn-sm btn-danger'), '', '<span class="glyphicon glyphicon-trash"></span> ') }}

                                </td>
                            </tr>
                            <?php endforeach; ?>
            
                        </tbody>
                    </table>
     
          </div>
        </div>
        <!-- content body end -->


<script type="text/javascript">

/* Table initialisation */
$(document).ready(function() {
  $('#myDataTable').dataTable( {
    "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
    "sPaginationType": "bootstrap",
    "oLanguage": {
      "sLengthMenu": "_MENU_ records per page"
    },
    "aLengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
    "aoColumnDefs": 
        [ 
            { "bSortable": false, "aTargets": [ 2 ] }
        ],

    "aaSorting": [[ 0, "desc" ]]
  });
});

</script>

@stop