@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Links</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="/admin/link" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a>
                 </div>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>

            {{ Form::open(array('route' => 'admin.link.store', 'method' => 'POST', 'files'=>TRUE, 'id'=>'myForm', 'class'=>'form-horizontal', 'role'=>'form')) }}

                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-8">
                      {{ Form::text('title','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="link_url" class="col-sm-2 control-label">URL</label>
                    <div class="col-sm-8">
                      {{ Form::text('link_url','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                  <div class="form-group box_category_image">
                    <label for="short_description_en" class="col-sm-2 control-label">Link Banner</label>
                    <div class="col-sm-8">
                        <span class="btn btn-info btn-file">
                          Browse {{Form::file('link_banner', array('type'=>'file', 'id'=>'link_banner'))}}
                        </span>
                        <span class="suggest_image_size">
                          *Image size 325x103 Pixels
                        </span>
                    </div>
                  </div> 


                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
          
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-floppy-disk">
                                    Save
                                </span>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <span class="glyphicon glyphicon-floppy-remove">
                                    Reset
                                </span>
                            </button>
                   
                    </div>
                  </div>


            {{ Form::close() }}

                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    @if ($errors->any())
                        <ul>
                            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                        </ul>
                    @endif
                </div>
                </div>


          </div>
        </div>
        <!-- content body end -->

@stop