@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Project</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="/admin/project" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a>
                 </div>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>

            {{ Form::open(array('route' => 'admin.project.store', 'method' => 'POST', 'files'=>TRUE, 'id'=>'myForm', 'class'=>'form-horizontal', 'role'=>'form')) }}

                 <div class="form-group">
                    <label for="parent_id" class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-8">

                      <select id="projectcategories_id" name="projectcategories_id"  class="form-control">
                          @foreach($categories as $cat)
                            @if(empty($cat['children']))
                              <option value="{{$cat['id']}}">{{$cat['title']}}</option>
                            @else
                              <optgroup label="{{$cat['title']}}">
                                @foreach($cat['children'] as $k =>$v)
                                  <option value="{{$v['id']}}">{{$v['title']}}</option>
                                @endforeach
                              </optgroup>
                            @endif
                        @endforeach
                      </select>


                    </div>
                  </div>


                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-8">
                      {{ Form::text('title','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title_en" class="col-sm-2 control-label">Title[EN]</label>
                    <div class="col-sm-8">
                      {{ Form::text('title_en','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Area</label>
                    <div class="col-sm-8">
                      {{ Form::text('area','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title_en" class="col-sm-2 control-label">Area [EN]</label>
                    <div class="col-sm-8">
                      {{ Form::text('area_en','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Scope of Work</label>
                    <div class="col-sm-8">
                      {{ Form::text('scope','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title_en" class="col-sm-2 control-label">Scope of Work [EN]</label>
                    <div class="col-sm-8">
                      {{ Form::text('scope_en','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Client</label>
                    <div class="col-sm-8">
                      {{ Form::text('client','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title_en" class="col-sm-2 control-label">Client [EN]</label>
                    <div class="col-sm-8">
                      {{ Form::text('client_en','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title_en" class="col-sm-2 control-label">Contact Value</label>
                    <div class="col-sm-8">
                      {{ Form::text('contact_value','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="title_en" class="col-sm-2 control-label">Contact Value [EN]</label>
                    <div class="col-sm-8">
                      {{ Form::text('contact_value_en','', array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('description','', array('class'=>'form-control', 'rows'=>'3')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="description_en" class="col-sm-2 control-label">Description[EN]</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('description_en','', array('class'=>'form-control', 'rows'=>'3')) }}
                    </div>
                  </div>  

                  <div class="form-group">
                    <label for="img" class="col-sm-2 control-label">Image</label>
                    <div class="col-sm-8">
                    	<span class="btn btn-info btn-file">
                       		Browse File.. {{Form::file('img', array('type'=>'file', 'id'=>'img'))}}
                       	</span>
                    </div>
                  </div>  

                <!-- Upload & Crop Hidden Field  -->
                <div class="form-group">
                    <div class="col-sm-2 control-label"></div>
                    <div class="col-sm-8">
                      <input type="hidden" size="4" id="x1" name="x1" />
                      <input type="hidden" size="4" id="y1" name="y1" />
                      <input type="hidden" size="4" id="x2" name="x2" />
                      <input type="hidden" size="4" id="y2" name="y2" />
                      <input type="hidden" size="4" id="w" name="w" />
                      <input type="hidden" size="4" id="h" name="h" />

                      <!-- Config for jcrop -->
                      <input type="hidden" size="4" id="width_image" name="width_image" value="{{Config::get('admin.project.width')}}" />
                      <input type="hidden" size="4" id="height_image" name="height_image" value="{{Config::get('admin.project.height')}}" />
                      <input type="hidden" size="4" id="width_thumb" name="width_thumb" value="{{Config::get('admin.project.width_thumb')}}" />
                      <input type="hidden" size="4" id="height_thumb" name="height_thumb" value="{{Config::get('admin.project.height_thumb')}}" />

                      <!-- Config for post -->
                      <input type="hidden" name="image" id="image">
                      <input type="hidden" name="image_thumb" id="image_thumb">
                    
                      <div id="image_area"></div>

                      <br/>
                      <button type="button" class="btn btn-primary" onclick="cropImage();" name="crop_thumb">
                      <span class="glyphicon glyphicon-picture"></span>
                      Crop Image
                      </button>
                      <br/><br/>

                      <div id="image_thumb_area">
                        <img id="image_thumb" src="">
                      </div>
                    </div>
                </div>
                <!-- Upload & Crop Hidden Field  -->


                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
          
                            <button type="submit" class="btn btn-success" onClick="return checkCrop();">
                                <span class="glyphicon glyphicon-floppy-disk">
                                    Save
                                </span>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <span class="glyphicon glyphicon-floppy-remove">
                                    Reset
                                </span>
                            </button>
                   
                    </div>
                  </div>


            {{ Form::close() }}

                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    @if ($errors->any())
                        <ul>
                            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                        </ul>
                    @endif
                </div>
                </div>


          </div>
        </div>
        <!-- content body end -->


<!-- Start Script for upload & Crop -->

<script type="text/javascript">

  $(document).ready(function(){

    $('input#img').ajaxfileupload({
        'action': '/admin/uploadTemp',
        'params': {
          'extra': 'info',
        'width_image': $('#width_image').val(),
        'height_image': $('#height_image').val(),
        },
      'onStart': function() 
      {
        //if(weWantedTo) return false; // cancels upload
        $('#image_area').html('');
        // $('img#preview').attr('src', '');
      },
      'onComplete': function(resp) 
      {
        //do anything here as you want
        console.log(resp.img)
        //alert(JSON.stringify(response));
        var image_path = '/upload/temp_dir/';
        
        $('#image_area').append('<img id="imagex" src="' + image_path + resp.img + '"/>');
        $('input#image').attr('value', resp.img);
        // $('img#preview').attr('src', resp.img);

        $('img#imagex').Jcrop({
            boxWidth:   600, 
            onSelect:   showCoords,
            bgColor:    'black',
            bgOpacity:  .7,
            setSelect:  [ 0, 0, 120, 80 ],
            aspectRatio: 325/217
        });

      }

      });
  });


  function cropImage()
  {
    
      var image_path = '/upload/temp_dir/';

    $.ajax({
    url: '/admin/uploadCrop',
    type: 'POST',
    data: { 
        x1: $('#x1').val(), 
        y1: $('#y1').val(),
        x2: $('#x2').val(),
        y2: $('#y2').val(),
        w: $('#w').val(),
        h: $('#h').val(),
        width_thumb: $('#width_thumb').val(),
        height_thumb: $('#height_thumb').val(),
        image: $('#image').val()
    },
    success: function(resp) {
        //called when successful
      $('#image_thumb_area img#image_thumb').attr('src', image_path + resp.image_thumb + '?r=' + Math.random());
        $('input#image_thumb').attr('value', resp.image_thumb); 
        console.log('ok');
    },
    error: function(e) {
        //called when there is an error
      console.log(e.message);
    }

  });
  }

    function showCoords(c)
    {
      jQuery('#x1').val(c.x);
      jQuery('#y1').val(c.y);
      jQuery('#x2').val(c.x2);
      jQuery('#y2').val(c.y2);
      jQuery('#w').val(c.w);
      jQuery('#h').val(c.h);
    };

  function checkCrop()
  {
    if( $('#x1').val() =='' || $('#y1').val() == '' || $('#image_name_thumb').val() == '' ){
      alert('Please crop image before submit data');
      return false;
    }
  }


</script>
<!-- Start Script for upload & Crop -->

@stop