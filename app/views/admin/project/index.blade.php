@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Project</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="project/create" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-plus-sign"></span> Add
                    </a>
                </div>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">
          <div class="col-lg-12">
                    <hr class="thin" />

                    <table class="table table-striped table-bordered table-hover" id="myDataTable">
                        <thead>
                            <tr>
                            	<th class="running_number">ID</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th class="center width300">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($datas as $data): ?>
                            <tr class="odd gradeX">
                            	<td class="width70">{{$data->id}}</td>
                                <td>{{$data->title}}</td>
                                <td>{{$data->category_title}}</td>
                                <td class="width300 center">

                                    {{ HTML::link_nested('admin.projectgallery.show', 'Gallery', array('class'=>'btn btn-sm btn-info'), '', '<span class="glyphicon glyphicon-ok-sign"></span> ', array($data->id)) }}

                                    {{ HTML::link_nested('admin.project.show', 'View', array('class'=>'btn btn-sm btn-primary'), '', '<span class="glyphicon glyphicon-ok-sign"></span> ', array($data->id)) }}

                                    {{ HTML::link_nested('admin.project.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($data->id)) }}

                                    {{ HTML::delete_button('admin.project.destroy', $data->id, 'Delete', array('class'=>'btn btn-sm btn-danger'), '', '<span class="glyphicon glyphicon-trash"></span> ') }}

                                </td>
                            </tr>
                            <?php endforeach; ?>
            
                        </tbody>
                    </table>
     
          </div>
        </div>
        <!-- content body end -->


<script type="text/javascript">

/* Table initialisation */
$(document).ready(function() {
  $('#myDataTable').dataTable( {
    "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
    "sPaginationType": "bootstrap",
    "oLanguage": {
      "sLengthMenu": "_MENU_ records per page"
    },
    "aLengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
    "aoColumnDefs": 
        [ 
            { "bSortable": false, "aTargets": [ 3 ] }
        ],

    "aaSorting": [[ 0, "desc" ]]
  });
});

</script>

@stop