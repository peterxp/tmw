@extends('admin._layouts.master')

@section('content')

	<!-- content head start -->
	<div class="row">
		<div class="col-lg-12">
		 <div class="row">
		<div class="col-lg-10">
			<div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Product</div>
		</div>
		<div class="col-lg-2">
			<div class="pull-right">
				<a href="/admin/project" class="btn btn-primary " role="button">
					<span class="glyphicon glyphicon-chevron-left"></span> Back
				</a>
			 </div>   
		</div>
		</div>
		</div>
	</div>
	<!-- content head end-->

	<!-- content body start -->
	<div class="row">

	    <div class="col-lg-12">

		<hr class="thin" />

		<p>&nbsp;</p>


		<div class="panel panel-info">
		    <div class="panel-heading">Detail</div>
			<table class="table table-hover">
	
				<tbody>

						<tr>
						  	<td class="topic_left width_150">Title</td>
						  	<td>{{$data->title}}</td>
						</tr>

						<tr>
						  	<td class="topic_left width_150">Name[EN]</td>
						  	<td>{{$data->title_en}}</td>
						</tr>

						<tr>
						  	<td class="topic_left width_150">Category</td>
						  	<td>{{$data->category_title}}</td>
						</tr>
----------
						<tr>
						  	<td class="topic_left width_150">Area</td>
						  	<td>{{$data->area}}</td>
						</tr>
						<tr>
						  	<td class="topic_left width_150">Area [EN]</td>
						  	<td>{{$data->area_en}}</td>
						</tr>
						<tr>
						  	<td class="topic_left width_150">Scope of Work</td>
						  	<td>{{$data->scope}}</td>
						</tr>
						<tr>
						  	<td class="topic_left width_150">Scope of Work [EN]</td>
						  	<td>{{$data->scope_en}}</td>
						</tr>
						<tr>
						  	<td class="topic_left width_150">Client</td>
						  	<td>{{$data->client}}</td>
						</tr>
						<tr>
						  	<td class="topic_left width_150">Client [EN]</td>
						  	<td>{{$data->client_en}}</td>
						</tr>
						<tr>
						  	<td class="topic_left width_150">Contact Value</td>
						  	<td>{{$data->contact_value}}</td>
						</tr>
						<tr>
						  	<td class="topic_left width_150">Contact Value [EN]</td>
						  	<td>{{$data->contact_value_en}}</td>
						</tr>
												
						<tr>
						  	<td class="topic_left">Description</td>
						  	<td>{{$data->description}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Description[EN]</td>
						  	<td>{{$data->description_en}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Image</td>
						  	<td>
								@if(file_exists('upload/images/project/'.$data->image_thumb))
		                    		{{HTML::image('upload/images/project/'.$data->image_thumb, null, null)}}
		                    	@endif
						  	</td>
						</tr>

						<tr>
						  	<td class="topic_left">created_at</td>
						  	<td>{{$data->created_at}}</td>
						</tr>

						<tr>
							<td class="topic_left">updated_at</td>
							<td>{{$data->updated_at}}</td>
						</tr>

						<tr>
							<td class="topic_left">&nbsp;</td>
							<td>
							{{ HTML::link_nested('admin.project.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($data->id)) }}
							</td>
						</tr>

				
			  	</tbody>
			</table>
		 </div>

	  </div>
	</div>
	<!-- content body end -->

@stop