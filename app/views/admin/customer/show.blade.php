@extends('admin._layouts.master')

@section('content')

	<!-- content head start -->
	<div class="row">
		<div class="col-lg-12">
		 <div class="row">
		<div class="col-lg-10">
			<div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Custcustomer</div>
		</div>
		<div class="col-lg-2">
			<div class="pull-right">
				<a href="/admin/customer" class="btn btn-primary " role="button">
					<span class="glyphicon glyphicon-chevron-left"></span> Back
				</a>
			 </div>   
		</div>
		</div>
		</div>
	</div>
	<!-- content head end-->

	<!-- content body start -->
	<div class="row">

	    <div class="col-lg-12">

		<hr class="thin" />

		<p>&nbsp;</p>


		<div class="panel panel-info">
		    <div class="panel-heading">Detail</div>
			<table class="table table-hover">
	
				<tbody>
					
						<tr>
						  	<td class="topic_left width_150">Title</td>
						  	<td>{{$data->title}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Logo</td>
						  	<td>
							@if($data->customer_logo != "")
	                    		{{HTML::image('/upload/images/customer_logo/'.$data->customer_logo, null, null)}}
	                    	@else
	                    		-
	                    	@endif
						  	</td>
						</tr>
	
						<tr>
						  	<td class="topic_left">created_at</td>
						  	<td>{{$data->created_at}}</td>
						</tr>

						<tr>
							<td class="topic_left">updated_at</td>
							<td>{{$data->updated_at}}</td>
						</tr>

						<tr>
							<td class="topic_left">&nbsp;</td>
							<td>
							{{ HTML::link_nested('admin.customer.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($data->id)) }}
							</td>
						</tr>

				
			  	</tbody>
			</table>
		 </div>

	  </div>
	</div>
	<!-- content body end -->

@stop