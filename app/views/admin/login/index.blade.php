@extends('admin._layouts.login')

@section('content')
  
    <div class="row margin-top40">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">

				{{HTML::image('/images/who.png', null, array('class'=>'profile-img'))}}

                {{Form::open(array('url'=>'administrator/login', 'class'=>'form-signin'))}}
                <p class="profile-username">
                    {{Form::text('email', Input::old('email'),  array('class'=>'form-control', 'placeholder'=>'Email'))}}
                </p>
                <p class="profile-username">
                    {{Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password'))}}
                </p>
                <p>
                    {{Session::get('login_error')}}
                </p>
                <button class="btn btn-primary btn-block" type="submit">Log in</button>
                <a href="#" class="pull-right need-help"><!-- Forgot Password?  --></a><span class="clearfix"></span>
                {{Form::close()}}
            </div>
        </div>
    </div>

@stop