@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> User Profile</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">

                </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>




		<div class="panel panel-info">
		    <div class="panel-heading">Detail</div>
			<table class="table table-hover">
	               
            		<tbody>
					
						<tr>
						  	<td class="topic_left width_150">First Name</td>
						  	<td>{{$data->first_name}}</td>
						</tr>

						<tr>
						  	<td class="topic_left width_150">Last Name</td>
						  	<td>{{$data->last_name}}</td>
						</tr>

						<tr>
						  	<td class="topic_left width_150">Email</td>
						  	<td>{{$data->email}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Last Login</td>
						  	<td>{{$data->last_login}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">created_at</td>
						  	<td>{{$data->created_at}}</td>
						</tr>

						<tr>
							<td class="topic_left">updated_at</td>
							<td>{{$data->updated_at}}</td>
						</tr>

						<tr>
							<td class="topic_left">&nbsp;</td>
							<td>
							{{ HTML::link_nested('admin.user.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($data->id)) }}
							</td>
						</tr>

				
			  	</tbody>
			</table>
		 </div>


          </div>
        </div>
        <!-- content body end -->

@stop