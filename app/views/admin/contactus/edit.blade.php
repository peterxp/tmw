@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Contact Us</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">
                    <a href="/admin/contactus/1/edit" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a>
                 </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>



			{{ Form::open(array('method' => 'PATCH', 'files'=>TRUE, 'id'=>'myForm', 'class'=>'form-horizontal', 'role'=>'form', 'route' => array('admin.contactus.update', $data->id))) }}

                 <div class="form-group">
                    <label for="company_name" class="col-sm-2 control-label">Company Name</label>
                    <div class="col-sm-8">
                      {{ Form::text('company_name', $data->company_name, array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="company_name_en" class="col-sm-2 control-label">Company Name [EN]</label>
                    <div class="col-sm-8">
                       {{ Form::text('company_name_en', $data->company_name_en, array('class'=>'form-control')) }}
                    </div>
                  </div>

                 <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('address', $data->address, array('class'=>'form-control', 'rows'=>'3')) }}
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="address_en" class="col-sm-2 control-label">Address [EN]</label>
                    <div class="col-sm-8">
                      {{ Form::textarea('address_en', $data->address_en, array('class'=>'form-control', 'rows'=>'3')) }}
                    </div>
                  </div>                 

                 <div class="form-group">
                    <label for="province" class="col-sm-2 control-label">Province</label>
                    <div class="col-sm-8">
                       {{ Form::text('province', $data->province, array('class'=>'form-control')) }}
                    </div>
                  </div>
                 <div class="form-group">
                    <label for="province_en" class="col-sm-2 control-label">Province [EN]</label>
                    <div class="col-sm-8">
                       {{ Form::text('province_en', $data->province_en, array('class'=>'form-control')) }}
                    </div>
                  </div>
                 <div class="form-group">
                    <label for="post_code" class="col-sm-2 control-label">Post Code</label>
                    <div class="col-sm-8">
                       {{ Form::text('post_code', $data->post_code, array('class'=>'form-control')) }}
                    </div>
                  </div>
<!--                  <div class="form-group">
                    <label for="tax_id" class="col-sm-2 control-label">TAX ID</label>
                    <div class="col-sm-8">
                       {{ Form::text('tax_id', $data->tax_id, array('class'=>'form-control')) }}
                    </div>
                  </div> -->
                 <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-8">
                       {{ Form::text('email', $data->email, array('class'=>'form-control')) }}
                    </div>
                  </div>

                <div class="form-group">
                    <label for="facebook_url" class="col-sm-2 control-label">Facebook URL</label>
                    <div class="col-sm-8">
                       {{ Form::text('facebook_url', $data->facebook_url, array('class'=>'form-control')) }}
                    </div>
                  </div>

                <div class="form-group">
                    <label for="twitter_url" class="col-sm-2 control-label">Twitter URL</label>
                    <div class="col-sm-8">
                       {{ Form::text('twitter_url', $data->twitter_url, array('class'=>'form-control')) }}
                    </div>
                  </div>

                <div class="form-group">
                    <label for="instagram_url" class="col-sm-2 control-label">Instagram URL</label>
                    <div class="col-sm-8">
                       {{ Form::text('instagram_url', $data->instagram_url, array('class'=>'form-control')) }}
                    </div>
                  </div>
                <div class="form-group">
                    <label for="twitter_url" class="col-sm-2 control-label">Youtube URL</label>
                    <div class="col-sm-8">
                       {{ Form::text('youtube_url', $data->youtube_url, array('class'=>'form-control')) }}
                    </div>
                  </div>
                <div class="form-group">
                    <label for="telephone" class="col-sm-2 control-label">Telephone</label>
                    <div class="col-sm-8">
                       {{ Form::text('telephone', $data->telephone, array('class'=>'form-control')) }}
                    </div>
                  </div>
                <div class="form-group">
                    <label for="mobile" class="col-sm-2 control-label">Mobile</label>
                    <div class="col-sm-8">
                       {{ Form::text('mobile', $data->mobile, array('class'=>'form-control')) }}
                    </div>
                  </div>
                 <div class="form-group">
                    <label for="fax" class="col-sm-2 control-label">Fax</label>
                    <div class="col-sm-8">
                       {{ Form::text('fax', $data->fax, array('class'=>'form-control')) }}
                    </div>
                  </div>                 


                  <div class="form-group">
                    <label for="img" class="col-sm-2 control-label">Map</label>
                    <div class="col-sm-8">

                    	@if($data->map != "")
                    		<?php 
                    		  $tmp = explode('.', $data->map);
													$file_extension = end($tmp);
                    		?>
                    		@if($file_extension == "pdf")
													<!-- <img src="/upload/images/contactus/{{$data->map}}" alt="Map" class="map_thumb" /> -->
                    			<a class="facnybox_single" href="/upload/images/contactus/{{$data->map}}" target="_blank" title="Map">
													PDF Map
												</a>
                    		@else
	                    		<a class="facnybox_single" href="/upload/images/contactus/{{$data->map}}" title="Map">
														<img src="/upload/images/contactus/{{$data->map}}" alt="Map" class="map_thumb" />
													</a>
                    		@endif

                    	@endif
                      <span class="suggest_image_size">(User can upload file extension .PDF or .JPEG)</span>
                      
                    	<br/><br/>
                    	<span class="btn btn-info btn-file">
                       		Browse File.. {{Form::file('map', array('type'=>'file', 'id'=>'map'))}}
                       	</span>
                    </div>
                  </div>  
                 	

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
          
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-floppy-disk">
                                    Save
                                </span>
                            </button>
                            <button type="reset" class="btn btn-danger">
                                <span class="glyphicon glyphicon-floppy-remove">
                                    Reset
                                </span>
                            </button>
                   
                    </div>
                  </div>


            {{ Form::close() }}

                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    @if ($errors->any())
                        <ul>
                            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                        </ul>
                    @endif
                </div>
                </div>


          </div>
        </div>
        <!-- content body end -->

@stop