@extends('admin._layouts.master')

@section('content')

        <!-- content head start -->
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
            <div class="col-lg-10">
                <div class="topic_content"> <span class="glyphicon glyphicon-th-large"></span> Contact Us</div>
            </div>
            <div class="col-lg-2">
                <row class="pull-right">
                <!-- 
                    <a href="/admin/contactus/1/edit" class="btn btn-primary " role="button">
                        <span class="glyphicon glyphicon-chevron-left"></span> Back
                    </a>
                 -->
                 </row>   
            </div>
            </div>
            </div>
        </div>
        <!-- content head end-->

        <!-- content body start -->
        <div class="row">

          <div class="col-lg-12">

            <hr class="thin" />

            <p>&nbsp;</p>




		<div class="panel panel-info">
		    <div class="panel-heading">Detail</div>
			<table class="table table-hover">
	               
            		<tbody>
					
						<tr>
						  	<td class="topic_left width_150">Company Name</td>
						  	<td>{{$data->company_name}}</td>
						</tr>

						<tr>
						  	<td class="topic_left width_150">Company Name [EN]</td>
						  	<td>{{$data->company_name_en}}</td>
						</tr>

						<tr>
						  	<td class="topic_left width_150">Address</td>
						  	<td>{{$data->address}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Address [EN]</td>
						  	<td>{{$data->address_en}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Province</td>
						  	<td>{{$data->province}}</td>
						</tr>
						<tr>
						  	<td class="topic_left">Province [EN]</td>
						  	<td>{{$data->province_en}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Post Code</td>
						  	<td>{{$data->post_code}}</td>
						</tr>
						
<!-- 						<tr>
						  	<td class="topic_left">TAX ID</td>
						  	<td>{{$data->tax_id}}</td>
						</tr> -->

						<tr>
						  	<td class="topic_left">Email</td>
						  	<td>{{$data->email}}</td>
						</tr>
						<tr>
						  	<td class="topic_left">Facebook URL</td>
						  	<td>{{$data->facebook_url}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Twitter URL</td>
						  	<td>{{$data->twitter_url}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Instagram URL</td>
						  	<td>{{$data->instagram_url}}</td>
						</tr>
						<tr>
						  	<td class="topic_left">Youtube URL</td>
						  	<td>{{$data->youtube_url}}</td>
						</tr>

						<tr>
						  	<td class="topic_left">Telephone</td>
						  	<td>{{$data->telephone}}</td>
						</tr>
						<tr>
						  	<td class="topic_left">Mobile</td>
						  	<td>{{$data->mobile}}</td>
						</tr>
						<tr>
						  	<td class="topic_left">Fax</td>
						  	<td>{{$data->fax}}</td>
						</tr>
						
						

						<tr>
						  	<td class="topic_left">Image Map</td>
						  	<td>
	

              	@if($data->map != "")
              		<?php 
              		  $tmp = explode('.', $data->map);
										$file_extension = end($tmp);
              		?>
              		@if($file_extension == "pdf")
              			<a class="facnybox_single" href="/upload/images/contactus/{{$data->map}}" target="_blank" title="Map">
										PDF Map
									</a>
              		@else
                		<a class="facnybox_single" href="/upload/images/contactus/{{$data->map}}" title="Map">
											<img src="/upload/images/contactus/{{$data->map}}" alt="Map" class="map_thumb" />
										</a>
              		@endif
              		
              	@endif

						  	</td>
						</tr>

						<tr>
						  	<td class="topic_left">created_at</td>
						  	<td>{{$data->created_at}}</td>
						</tr>

						<tr>
							<td class="topic_left">updated_at</td>
							<td>{{$data->updated_at}}</td>
						</tr>

						<tr>
							<td class="topic_left">&nbsp;</td>
							<td>
							{{ HTML::link_nested('admin.contactus.edit', 'Edit', array('class'=>'btn btn-sm btn-warning'), '', '<span class="glyphicon glyphicon-edit"></span> ', array($data->id)) }}
							</td>
						</tr>

				
			  	</tbody>
			</table>
		 </div>


          </div>
        </div>
        <!-- content body end -->

@stop