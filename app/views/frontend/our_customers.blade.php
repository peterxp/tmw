@extends('frontend._layouts.master')

@section('content')

@include('frontend._layouts.header')  
@include('frontend._layouts.menu')
@include('frontend._layouts.main_slider')

<div class="content_box">

	<div class="main_content content_width row">
		
	<div class="content">
		<div class="main_topic_with_bullet marginBottom10">{{Lang::get('messages.our_customer')}}</div>

			<div class="aboutus_box">
				<!-- Start About Us Gallery -->
				<div class="our_customers_gallery">       		    
				    @foreach($data['customers'] as $customer)
				    <div class="col-xs-4 col-sm-4 col-md-4 our_customers_gallery">
				      	<div class="our_customers_thumbnail">
				        	<img src="/upload/images/customer_logo/{{$customer->customer_logo}}" title="{{$customer->title}}">
					      	<div class="text-center">
		              			<h4 class="company_name">{{$customer->title}}</h4>
		            		</div>
				   		</div>
				   	</div>
				   	@endforeach

			    </div>			    
				<!-- End About Us Gallery -->

			{{$data['customers']->links()}}
		</div>
		

	@include('frontend._layouts.social_box')

	</div>

	<!--Side Bar Start -->
	<div class="side_bar">
		@include('frontend._layouts.recent_projects_box')
		<br/>
		<br/>
		@include('frontend._layouts.link_box')
	</div>
	<!-- Side Bar End -->

	</div>

</div>

@stop