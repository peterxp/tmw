@extends('frontend._layouts.master')

@section('content')

@include('frontend._layouts.header')  
@include('frontend._layouts.menu')
@include('frontend._layouts.job_vacancy_background')

<div class="content_box">

	<div class="main_content content_width row">
		
	<div class="content">
		<div class="main_topic_with_bullet marginBottom10">{{Lang::get('messages.job_vacancy')}}</div>

			<div class="job_vacancy_box">

				<div class="panel-group" id="accordion" class="accordion">
					<?php $i = 0;?>
					@foreach($data['jobs'] as $job)
						<?php 
							$i++;
							$in = ($i == 1)?"in":"";
						?>
					  	<div class="panel panel-default myAccordion" id="myAccordion_{{$job->id}}">
						    <div class="panel-heading gu-eng" style="background-color:#c9c9c9">
						      	<h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$job->id}}">
									@if(Session::get('lang') == "en")
										{{$job->topic_en}}
									@else
										{{$job->topic}}
									@endif
						        </a>
						      </h4>
						    </div>
						    <div id="collapse_{{$job->id}}" class="panel-collapse collapse {{$in}}">
						      	<div class="panel-body">
						      		@if(Session::get('lang') == "en")
										{{$job->detail_en}}
									@else
										{{$job->detail}}
									@endif
								</div>
						    </div>
					  	</div>
				  	@endforeach

					</div>

			</div>
		

	@include('frontend._layouts.social_box')

	</div>

	<!--Side Bar Start -->
	<div class="side_bar">
		@include('frontend._layouts.recent_projects_box')
		<br/>
		<br/>
		@include('frontend._layouts.link_box')
	</div>
	<!-- Side Bar End -->

	</div>

</div>

@stop