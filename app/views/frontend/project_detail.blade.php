@extends('frontend._layouts.overlay')
<style type="text/css">

html body{
		background-image: url('');
	}
	.overlay_box{
		margin: 0 auto;
		padding:0px;
		width: 840px;
 		height: 100%; 
  		position: relative;
		/*border: 1px solid #f00;*/
		/*background-color: #e7e7e7;*/
	}

	.overlay_box_left{
		float: left;
		width: 540px;
		margin: 25px 10px 0px 20px;
		display: inline-block;
	}

	.overlay_box_right{
		float: left;
		width: 250px;
		margin: 25px 10px 0px 10px;
		line-height: 150%;
	}

	/* Start Gallery */

	#thumbs_projectgallery { 
		padding-top: 10px;
		overflow: hidden; 
		/*background: #eee;*/
	}
	#largeImage {
		max-width: 540px;
		height: auto;
	}

	#thumbs_projectgallery img, #largeImage {
	 /*border: 1px solid gray;*/
	 padding: 0px;
	 background-color: none;
	 cursor: pointer;
	}
	#thumbs_projectgallery img {
		float: left;
		width: 100px;
		height: 100px;
		border: 3px solid #FDFCFC;
		padding: 2px;
		margin: 0px 2px 5px 4px;
	}
	#panel_product { position: relative; }
	/*Eng Gallery*/

	#gallery_project .description{
		background: #d0d0d0;
		width: 640px;
		padding: 10px 5px;
		color: #073783;
	}

	.project_name_underline {
		font-size: 17px;
		font-weight: 700;
		padding: 0px 0px 5px 0px;
		margin: 0px 0px 5px 0px;
		border-bottom: 1px solid #aaa;
	}
	.project_area{
		font-size: 12px;
		font-weight: 700;
		padding: 0px 0px 5px 0px;
		margin: 0px 0px 5px 0px;		
	}

</style>

@section('content')
	
<div class="overlay_box">

	<!-- Start left box -->
	<div class="overlay_box_left">
		<!-- Start Gallery -->
		<!-- <div id="gallery_project"> -->
		    <div id="panel_product">
		        <img id="largeImage" src="" />
		    </div>
		    <div id="thumbs_projectgallery">
		   		<?php foreach ($data['project_gallery'] as $gal): ?>
		    	<img src="/upload/images/projectgallery/{{$gal->image_thumb}}" />
		    <?php endforeach; ?>

		    </div>
		<!-- </div> -->
		<!-- End Gallery -->
	</div>

	<!-- Start right box -->
	<div class="overlay_box_right">
		<?php foreach ($data['projects'] as $pro): ?>
			@if(Session::get('lang') == "en")
				<div class="project_name_underline"> {{$pro->title_en}}</div>
				<div class="project_area"> {{$pro->area_en}}</div>
		    	<div class="project_type"><b>Type:</b> {{$pro->category_title_en}}</div>
		    	<div class="project_type"><b>Scope of Work:</b> {{$pro->scope_en}}</div>
		    	<div class="project_type"><b>Client:</b> {{$pro->client_en}}</div>
		    	<div class="project_type"><b>Contact Value:</b> {{$pro->contact_value_en}}</div>
		    	<div class="description"><b>Description:</b> {{$pro->description_en}}</div>
			@else
				<div class="project_name_underline"> {{$pro->title}}</div>
				<div class="project_area"> {{$pro->area}}</div>
		    	<div class="project_type"><b>Type:</b> {{$pro->category_title}}</div>
		    	<div class="project_type"><b>Scope of Work:</b> {{$pro->scope}}</div>
		    	<div class="project_type"><b>Client:</b> {{$pro->client}}</div>
		    	<div class="project_type"><b>Contact Value:</b> {{$pro->contact_value}}</div>
		    	<div class="description"><b>Description:</b> {{$pro->description}}</div>
			@endif
		<?php endforeach; ?>
	</div>


</div>


<script type="text/javascript">
	$(document).ready(function(){

		$('#thumbs_projectgallery').delegate('img','click', function(){
		    $('#largeImage').attr('src',$(this).attr('src').replace('thumb_',''));
		});

		/* Default Large Image */
		$('#largeImage').attr('src', $('#thumbs_projectgallery img:first').attr('src').replace('thumb_',''));

	});
</script>

@stop