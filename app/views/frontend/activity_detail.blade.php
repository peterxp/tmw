@extends('frontend._layouts.overlay')
<style type="text/css">

html body{
		background-image: url('');
	}
	.overlay_box{
		margin: 0 auto;
		padding:0px;
		width: 650px;
		height: auto;
		/*border: 1px solid #f00;*/
		background-color: #e7e7e7;
	}

	.overlay_box_left{
		float: left;
		margin: 25px 10px 0px 10px;
		display: inline-block;
		/*border:1px solid #00f;*/
	}


	/* Start Gallery */

	#thumbs_activitygallery { 
		padding-top: 10px;
		overflow: hidden; 
	}
	#largeImage {
		max-width: 640px;
		height: auto;
	}

	#thumbs_activitygallery img, #largeImage {
	 /*border: 1px solid gray;*/
	 padding: 0px;
	 background-color: none;
	 cursor: pointer;
	}
	#thumbs_activitygallery img {
		float: left;
		width: 100px;
		height: 100px;
		border: 3px solid #FDFCFC;
		padding: 2px;
		margin: 0px 2px 5px 4px;
	}
	#panel_product { position: relative; }
	/*Eng Gallery*/

	#gallery_activity .description{
		background: #d0d0d0;
		width: 640px;
		padding: 10px 5px;
		color: #073783;
	}

</style>

@section('content')
	
	<div class="overlay_box">
	<div class="overlay_box_left">

		<!-- Start Gallery -->
		<div id="gallery_activity">
		    <div id="panel_product">
		        <img id="largeImage" src="" />
		    </div>
		    
		    <?php foreach ($data['activities'] as $ac): ?>
		    	<div class="description">
		    		@if(Session::get('lang') == "en")
				  		{{$ac->description_en}}
				  	@else
						{{$ac->description}}
					@endif
  				</div>
			<?php endforeach; ?>

		    <div id="thumbs_activitygallery">
		    	<?php foreach ($data['activity_gallery'] as $data): ?>
		        <img src="/upload/images/activitygallery/{{$data->image_thumb}}" />
		      <?php endforeach; ?>

		    </div>
		</div>
		<!-- End Gallery -->
	</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#thumbs_activitygallery').delegate('img','click', function(){
		    $('#largeImage').attr('src',$(this).attr('src').replace('thumb_',''));
		});

		/* Default Large Image */
		$('#largeImage').attr('src', $('#thumbs_activitygallery img:first').attr('src').replace('thumb_',''));

	});
</script>

@stop