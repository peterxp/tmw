@extends('frontend._layouts.master')

@section('content')

@include('frontend._layouts.header')  
@include('frontend._layouts.menu')
@include('frontend._layouts.main_slider')

<div class="content_box">

	<div class="main_content content_width row">
		
	<div class="content">
		<div class="main_topic_with_bullet marginBottom10">{{Lang::get('messages.aboutus')}}</div>

			<div class="aboutus_box">
			@foreach($data['aboutus'] as $ab)
				<!-- Start About Us Big -->
				<div class="what_we_do">
						<img src="/upload/images/aboutus/{{$ab->image}}"/>
						@if(Session::get('lang') == "en")
							<div class="main_topic">{{$ab->title_en}}</div>
							<p>{{$ab->description_en}}</p>
						@else
							<div class="main_topic">{{$ab->title}}</div>
							<p>{{$ab->description}}</p>
						@endif
				</div>
				<!-- End About Us Big -->
						
				<!-- Start About Us Gallery -->
				<div class="aboutus_gallery">       
			    
			    <div class="col-xs-3 col-sm-3 col-md-3 aboutus_list_gallery">
			      <div class="aboutus_thumbnail">
			        <img src="/upload/images/aboutus/{{$ab->image1}}" alt="">
			      </div>
			    </div>
			    
			    <div class="col-xs-3 col-sm-3 col-md-3 aboutus_list_gallery">
			      <div class="aboutus_thumbnail">
			        <img src="/upload/images/aboutus/{{$ab->image2}}" alt="">
			      </div>
			    </div>    

			    <div class="col-xs-3 col-sm-3 col-md-3 aboutus_list_gallery">
			      <div class="aboutus_thumbnail">
			        <img src="/upload/images/aboutus/{{$ab->image2}}" alt="">
			      </div>
			    </div>  

			    <div class="col-xs-3 col-sm-3 col-md-3 aboutus_list_gallery">
			      <div class="aboutus_thumbnail">
			        <img src="/upload/images/aboutus/{{$ab->image4}}" alt="">
			      </div>
			    </div>  

				</div>
				<!-- End About Us Gallery -->
			@endforeach
			</div>
		
	@include('frontend._layouts.social_box')

	</div>

	<!--Side Bar Start -->
	<div class="side_bar">
		@include('frontend._layouts.recent_projects_box')
		<br/>
		<br/>
		@include('frontend._layouts.link_box')
	</div>
	<!-- Side Bar End -->

	</div>

</div>

@stop