@extends('frontend._layouts.master')

@section('content')

@include('frontend._layouts.header')  
@include('frontend._layouts.menu')
@include('frontend._layouts.main_slider')

<div class="content_box">

	<div class="main_content content_width row">
		
	<div class="content">
		<div class="main_topic_with_bullet marginBottom10">{{Lang::get('messages.services')}}</div>

			<div class="services_list_box">
			   	
			   	@foreach($categories as $cat)
			   		<div class="services_list_item">
				   		<img src="/upload/images/projectcategory_icon/{{$cat['image']}}" alt="post img"/>
				     	@if(Session::get('lang') == "en")
					     	<h4 class="services_list_topic">{{$cat['title_en']}}</h4>
					     	{{$cat['description_en']}}
					     	<p>
					     	@if(!empty($cat['children']))
					     		@foreach($cat['children'] as $k =>$v)
					     			<div class="project_subcat"><span class="glyphicon glyphicon-ok-sign"></span> {{$v['title_en']}}</div>
					     		@endforeach
					     	@endif
					     	<p>
				  		@else
					     	<h4 class="services_list_topic">{{$cat['title']}}</h4>
					     	{{$cat['description']}}
					     	<p>
					     	@if(!empty($cat['children']))
					     		@foreach($cat['children'] as $k =>$v)
					     			<div class="project_subcat"><span class="glyphicon glyphicon-ok-sign"></span> {{$v['title']}}</div>
					     		@endforeach
					     	@endif
					     	<p>
				  		@endif     	
			   		</div>
			   	@endforeach

			</div>
		

	@include('frontend._layouts.social_box')

	</div>

	<!--Side Bar Start -->
	<div class="side_bar">
		@include('frontend._layouts.recent_projects_box')
		<br/>
		<br/>
		@include('frontend._layouts.link_box')
	</div>
	<!-- Side Bar End -->

	</div>

</div>

@stop