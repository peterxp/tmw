@extends('frontend._layouts.master')

@section('content')

@include('frontend._layouts.header')  
@include('frontend._layouts.menu')
@include('frontend._layouts.main_slider')

<div class="content_box">

	<div class="main_content content_width row">
		
	<div class="content">
			
		<!-- Start About Us Gallery -->
		<div class="main_topic_with_bullet marginBottom10">{{Lang::get('messages.portfolio')}}</div>
		
		<div class="aboutus_box">

			<!-- Start Select Box -->
				<div class="row">	
					<div class="col-xs-4 col-sm-4 col-md-4">
					<form class="form-horizontal" role="form">
						<select id="project_category"  class="form-control">
						  	<option value="0">{{Lang::get('general_msg.all')}}</option>

						  	@if(Session::get('lang') == "en")
							  	@foreach($categories as $cat)
							  		<?php $sel = (Request::segment(3) == $cat['id'])?"selected":"";?>

								  	@if(empty($cat['children']))
							     		<option value="{{$cat['id']}}" {{$sel}} class="{{Helpers::formatUrl($cat['title_en'])}}">{{$cat['title_en']}}</option>
							     	@else
							     		<optgroup label="{{$cat['title_en']}}">
								     		@foreach($cat['children'] as $k =>$v)
								     			<?php $sel2 = (Request::segment(3) == $v['id'])?"selected":"";?>
								     			<option value="{{$v['id']}}" {{$sel2}} class="{{Helpers::formatUrl($v['title_en'])}}">{{$v['title_en']}}</option>
								     		@endforeach
							     		</optgroup>
							     	@endif
							 	@endforeach

							@else 

							  	@foreach($categories as $cat)
							  		<?php $sel = (Request::segment(3) == $cat['id'])?"selected":"";?>

								  	@if(empty($cat['children']))
							     		<option value="{{$cat['id']}}" {{$sel}} class="{{Helpers::formatUrl($cat['title_en'])}}">{{$cat['title']}}</option>
							     	@else
							     		<optgroup label="{{$cat['title']}}">
								     		@foreach($cat['children'] as $k =>$v)
								     			<?php $sel2 = (Request::segment(3) == $v['id'])?"selected":"";?>
								     			<option value="{{$v['id']}}" {{$sel2}} class="{{Helpers::formatUrl($v['title'])}}">{{$v['title']}}</option>
								     		@endforeach
							     		</optgroup>
							     	@endif
							 	@endforeach

							@endif


						</select>
					</form>	
					</div>
				</div>	
			<!-- End Select Box -->

			<div class="portfolio_list">       
		    @foreach($data['projects'] as $pro)
		    <div class="col-xs-4 col-sm-4 col-md-4 portfolio_list_thumbnail">
		      	<div class="portfolio_list">
		      		<a class="fancybox_overlay fancybox.ajax" data-fancybox-type="ajax" href="{{URL::to('/')}}/{{Request::segment(1)}}/project_detail/{{$pro->id}}">
			        	<img src="/upload/images/project/{{$pro->image}}" alt="">
				      	<div class="text-left">
	              			<h4 class="company_name">
	              			@if(Session::get('lang') == "en")
					  			{{$pro->title_en}}
					  		@else
								{{$pro->title}}
					  		@endif
	              			</h4>
	            		</div>
	            	</a>
		      	</div>
		    </div>
		    @endforeach    
			</div>
			{{$data['projects']->links()}}
		</div>
		<!-- End About Us Gallery -->

	@include('frontend._layouts.social_box')

	</div>

	<!--Side Bar Start -->
	<div class="side_bar">
		@include('frontend._layouts.recent_projects_box')
		<br/>
		<br/>
		@include('frontend._layouts.link_box')
	</div>
	<!-- Side Bar End -->

	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){

		$("#project_category").change(function() {

			var portfolio_cat_id	= $(this).val();
			// var portfolio_cat_name = $("select#project_category option:selected").text();
			var portfolio_cat_name = $("select#project_category option:selected").attr('class');
			 		  
			if(portfolio_cat_id != 0){
				var url = '/{{Request::segment(1)}}/portfolio/'+portfolio_cat_id+'/'+portfolio_cat_name;
			}else{
				var url = '/{{Request::segment(1)}}/portfolio';
			}

			window.location = url;
			//console.log(url);
		  
		});

	});
</script>
@stop