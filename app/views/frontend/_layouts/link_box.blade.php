Links Start -->
  <div class="box_link">
  	<div class="main_activity_topic">LINKS</div>

  		@foreach($data['links'] as $link)
  			<div class="link_banner">
	  			<a href="{{$link->link_url}}" target="_blank"><img src="/upload/images/link_banner/{{$link->link_banner}}"/></a>
	  		</div>
	  	@endforeach
  </div>
<!-- Links End