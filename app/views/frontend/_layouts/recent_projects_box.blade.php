	<!-- Acitivties Start -->		
		<div id="recentBox" class="recentBox"> 
			<div class="main_activity_topic underline_topic">{{Lang::get('messages.recent_projects')}}</div>

			@foreach($data['recent_project'] as $pro)
				<div class=" recentItem">
					<a class="fancybox_overlay fancybox.ajax" data-fancybox-type="ajax" href="{{URL::to('/')}}/{{Request::segment(1)}}/project_detail/{{$pro->id}}">
				   		<img src="/upload/images/project/{{$pro->image_thumb}}" alt="post img" class="pull-left"/>
				   		@if(Session::get('lang') == "en")
					      	<div class="item_topic_activity">{{$pro->title_en}}</div>
					     	<div>{{Str::limit($pro->description_en, 75, '..')}}</div>
					    @else
					      	<div class="item_topic_activity">{{$pro->title}}</div>
					     	<div>{{Str::limit($pro->description, 75, '..')}}</div>
					    @endif
			      	</a>
			   	</div>
			@endforeach	   
	 			<a class="btn-view-all displayBlock pull-right marginRight5" href="/{{Request::segment(1)}}/portfolio"> View All</a>
    	
	  </div>
  	<!-- Activities End