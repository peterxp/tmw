<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>TMW</title>
    <!-- web font -->
		<!-- 		<link href='http://fonts.googleapis.com/css?family=Share:400,400italic,700italic,700' rel='stylesheet' type='text/css'>
     -->

    {{ HTML::style('packages/bootstrap/css/bootstrap.min.css') }}
    {{ HTML::style('css/admin/font-awesome/css/font-awesome.min.css') }}
    {{HTML::style('packages/fancybox/source/jquery.fancybox.css')}}
    {{HTML::style('packages/bxslider/jquery.bxslider.css')}}
    {{HTML::style('css/frontend/style.css')}}
    {{HTML::style('css/admin/font-awesome/4.1.0/css/font-awesome.min.css')}}

    <!-- JavaScript -->
    {{HTML::script('packages/jquery/jquery.min.js')}}
    {{HTML::script('packages/bootstrap/js/bootstrap.min.js')}}
    {{HTML::script('packages/jquery-cookie/jquery_cookie.js') }}
    {{HTML::script('js/custom_function.js')}}
    {{HTML::script('packages/fancybox/source/jquery.fancybox.pack.js')}}
    {{HTML::script('packages/bxslider/jquery.bxslider.min.js')}} 
    
  	</head>

<body>

<div class="container">
        @yield('content')
        @include('frontend._layouts.footer')
  </div>
<script type="text/javascript">

$(document).ready(function() {

    // Project
    $(".fancybox_overlay").fancybox({
        maxWidth    : 840,
        maxHeight   : 600,
        fitToView   : false,
        width       : '70%',
        height      : '100%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
    });

    // Activity
    $(".fancybox_overlay_activity").fancybox({
        maxWidth    : 680,
        maxHeight   : 700,
        fitToView   : false,
        width       : '70%',
        height      : '100%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
    });

});

</script>
</body>
</html>
