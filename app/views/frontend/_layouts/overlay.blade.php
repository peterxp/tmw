<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CERNIC INTERNATIONAL</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('packages/bootstrap/css/bootstrap.css') }}
    {{ HTML::style('css/admin/font-awesome/css/font-awesome.min.css') }}




    <!-- JavaScript -->
    {{HTML::script('packages/jquery/jquery.min.js')}}
    {{HTML::script('packages/bootstrap/js/bootstrap.min.js')}}

    <!-- jQTree -->
    {{ HTML::style('packages/jqtree/jqtree.css') }}
    {{ HTML::script('packages/jqtree/tree.jquery.js') }}
    {{ HTML::script('packages/jquery-cookie/jquery_cookie.js') }}

    <!-- FancyBox -->
    {{HTML::style('packages/fancybox/source/jquery.fancybox.css')}}
    {{HTML::script('packages/fancybox/source/jquery.fancybox.pack.js')}}

    <!-- Add custom CSS here -->
    {{ HTML::style('css/frontend/style.css') }}
    {{ HTML::style('css/frontend/style_en.css') }}

  	</head>

<body>

    <div class="overlay_box">

          @yield('content')
  
    </div>


</body>
</html>
