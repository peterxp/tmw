<!-- Start Menu -->
<div class="main_menu_box">
		<nav id="topNav">
    	<ul>
        	<li><a href="/{{Request::segment(1)}}/home" class="@if(Request::segment(2) == 'home')topNav_active@endif">{{Lang::get('messages.home')}}</a></li>
      		<li><a href="/{{Request::segment(1)}}/services" class="@if(Request::segment(2) == 'services')topNav_active@endif">{{Lang::get('messages.services')}}</a></li>
      		<li><a href="/{{Request::segment(1)}}/portfolio" class="@if(Request::segment(2) == 'portfolio')topNav_active@endif">{{Lang::get('messages.portfolio')}}</a>
            	<ul>
                  @foreach($data['project_category'] as $pro)
                    @if(Session::get('lang') == "en")
                      <li><a href="/{{Request::segment(1)}}/portfolio/{{$pro->id}}/{{Helpers::formatUrl($pro->title_en)}}">{{$pro->title_en}}</a></li>
                    @else
                      <li><a href="/{{Request::segment(1)}}/portfolio/{{$pro->id}}/{{$pro->title}}">{{$pro->title}}</a></li>
                    @endif
                  @endforeach
                </ul>        
            </li>
          <li><a href="/{{Request::segment(1)}}/activities" class="@if(Request::segment(2) == 'activities')topNav_active@endif">{{Lang::get('messages.activities')}}</a></li>
          <li><a href="/{{Request::segment(1)}}/our_customers" class="@if(Request::segment(2) == 'our_customers')topNav_active@endif">{{Lang::get('messages.our_customer')}}</a></li>
          <li><a href="/{{Request::segment(1)}}/aboutus" class="@if(Request::segment(2) == 'aboutus')topNav_active@endif">{{Lang::get('messages.aboutus')}}</a></li>
          <li><a href="/{{Request::segment(1)}}/job-vacancy" class="@if(Request::segment(2) == 'job-vacancy')topNav_active@endif">{{Lang::get('messages.job_vacancy')}}</a></li>
          <li><a href="/{{Request::segment(1)}}/contactus" class="@if(Request::segment(2) == 'contactus')topNav_active@endif">{{Lang::get('messages.contactus')}}</a></li>
      </ul>
    </nav>
</div>
<!-- End Menu -->