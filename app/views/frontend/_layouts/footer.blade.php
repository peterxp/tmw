<div class="footer_box">
	<div class="footer content_width">

				@foreach($footer['address'] as $k => $val)
			    	@if(Session::get('lang') == "en")
				    				    	
						<div class="footer_left">
							{{$val->company_name_en}}<br/>
							{{$val->address_en}}
							{{$val->province_en}} {{$val->post_code}}<br/>
							Tel : {{$val->telephone}}  
							Fax : {{$val->fax}}
						</div>

						<div class="footer_right">
							Copyright @ 2013 THONGMIT WATTANAKITRUNGRUANG.<br/>
							All rights reserved 2013.<br/>
							Powered By <a href="http://www.websitemobileapp.com" target="_blank" class="link_white">www.websitemobileapp.com</a>
						</div>

				    @else

						<div class="footer_left">
							{{$val->company_name}}<br/>
							{{$val->address}}
							{{$val->province}} {{$val->post_code}}<br/>
							Tel : {{$val->telephone}}  
							Fax : {{$val->fax}}
						</div>

						<div class="footer_right">
							Copyright @ 2013 THONGMIT WATTANAKITRUNGRUANG.<br/>
							All rights reserved 2013.<br/>
							Powered By <a href="http://www.websitemobileapp.com" target="_blank" class="link_white">www.websitemobileapp.com</a>
						</div>

				    @endif

			    @endforeach

	</div>
</div>