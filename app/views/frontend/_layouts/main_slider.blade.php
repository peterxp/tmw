<div class="slider_box">
	<div id="my-carousel" class="carousel slide" data-ride="carousel">
	
		  	<ol class="carousel-indicators">
			  	<?php $n = 0;?>
			  	@foreach($data['slider'] as $nav)
				    <li data-target="#my-carousel" data-slide-to="{{$n}}" class="@if($n == 1)active@endif"></li>
				    <?php $n++?>
			    @endforeach
		  	</ol>
		  
		  	<div class="carousel-inner">
		  		<?php $s = 0;?>
		  		@foreach($data['slider'] as $der)
		  			<?php $s++?>
				    <div class="item @if($s == 1)active@endif">
				      <img src="/upload/images/slider/{{$der->image_thumb}}" alt="...">
				      <div class="carousel-caption">
				      		@if(Session::get('lang') == "en")
				         		<h3>{{$der->title_en}}</h3>
				         		<p>{{$der->description_en}}</p>
				         	@else
			         			<h3>{{$der->title}}</h3>
				         		<p>{{$der->description}}</p>
				         	@endif
				      </div>
				    </div>
			   	@endforeach
		  	</div>

	</div> 
</div>