<!-- Social Start -->
	<div class="main_topic">FOLLOW US</div>
	<div class="social_button_box">
		@foreach($footer['address'] as $k => $val)
			<a href="{{$val->facebook_url}}" title="Facebook" target="_blank">
				<div class="social_button facebook_button"></div>
			</a>
			<a href="{{$val->twitter_url}}" title="Twitter" target="_blank">
				<div class="social_button twitter_button"></div>
			</a>
			<a href="{{$val->instagram_url}}" title="Instragram" target="_blank">
				<div class="social_button instragram_button"></div>
			</a>
			<a href="{{$val->youtube_url}}" title="Youtube" target="_blank">
				<div class="social_button youtube_button"></div>
			</a>		
			<!--
			<a href="" title="Skype">
				<div class="social_button skype_button"></div>
			</a> -->	
		@endforeach					
	</div>
	<!-- Social End -->