	<!-- Acitivties Start -->		
		<div id="recentBox" class="recentBox"> 
			<div class="main_activity_topic underline_topic">{{Lang::get('messages.activities')}}</div>

			@foreach($data['sidebar_activity'] as $sba)
				<div class=" recentItem">
					<a class="fancybox_overlay_activity fancybox.ajax" data-fancybox-type="ajax" href="{{URL::to('/')}}/{{Request::segment(1)}}/activity_detail/{{$sba->id}}">
				   		<img src="/upload/images/activity/{{$sba->image_thumb}}" alt="post img" class="pull-left"/>
				   		@if(Session::get('lang') == "en")
					      	<div class="item_topic_activity">{{$sba->title_en}}</div>
					     	<div>{{$sba->description_en}}</div>
					    @else
					      	<div class="item_topic_activity">{{$sba->title}}</div>
					     	<div>{{$sba->description}}</div>
					    @endif
			      	</a>
			   	</div>
			@endforeach	   
	 			<a class="btn-view-all displayBlock pull-right marginRight5" href="/{{Request::segment(1)}}/activities"> View All</a>
 
	  </div>
  	<!-- Activities End