@extends('frontend._layouts.master')

@section('content')

@include('frontend._layouts.header')  
@include('frontend._layouts.menu')
@include('frontend._layouts.main_slider')

<div class="content_box">

	<div class="main_content content_width row">
		
	<div class="content">
			
		<!-- Start About Us Gallery -->
		<div class="main_topic_with_bullet marginBottom10">{{Lang::get('messages.activities')}}</div>
		
		<div class="aboutus_box">		
			<div class="portfolio_list">       
		    @foreach($data['activities'] as $pro)
		    <div class="col-xs-4 col-sm-4 col-md-4 portfolio_list_thumbnail">
		      	<div class="portfolio_list">
		      		<a class="fancybox_overlay_activity fancybox.ajax" data-fancybox-type="ajax" href="activity_detail/{{$pro->id}}">
		        		<img src="/upload/images/activity/{{$pro->image_thumb}}" alt="">
		        	</a>
			      	<div class="text-left">
              			<h4 class="company_name">
              			@if(Session::get('lang') == "en")
				  			{{$pro->title_en}}
				  		@else
							{{$pro->title}}
				  		@endif
              			</h4>
<!--               			<div class="short_desc">
              			@if(Session::get('lang') == "en")
				  			{{$pro->description_en}}
				  		@else
							{{$pro->description}}
				  		@endif
				  		</div> -->
            		</div>
		      	</div>
		    </div>
		    @endforeach    
			</div>
			{{$data['activities']->links()}}
		</div>
		<!-- End About Us Gallery -->

	@include('frontend._layouts.social_box')

	</div>

	<!--Side Bar Start -->
	<div class="side_bar">
		@include('frontend._layouts.recent_projects_box')
		<br/>
		<br/>
		@include('frontend._layouts.link_box')
	</div>
	<!-- Side Bar End -->

	</div>

</div>

@stop