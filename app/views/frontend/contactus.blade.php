@extends('frontend._layouts.master')

@section('content')

@include('frontend._layouts.header')  
@include('frontend._layouts.menu')
@include('frontend._layouts.contactus_background')

<div class="content_box">

	<div class="main_content content_width row">
		
	<div class="content">

		<div class="main_topic_with_bullet">{{Lang::get('messages.contactus')}}</div>

		<br/>
		<ul class="iconlist">


			<li class="pin">
				<div class="fontBold">ADDRESS:</div>
				
				@foreach($footer['address'] as $k => $val)
			    	@if(Session::get('lang') == "en")
				    	{{$val->address_en}}
				    	{{$val->province_en}}
				    @else
				    	{{$val->address}}
				    	{{$val->province}}
				    @endif
				    	{{$val->post_code}}
			    @endforeach

			</li>
			<li class="phone">
				<div class="fontBold">TEL:</div>
				{{$val->telephone}}
			</li>
			<li class="fax">
				<div class="fontBold">FAX:</div>
				{{$val->fax}}
			</li>
			<li class="mail">
				<div class="fontBold">Email:</div>
				{{$val->email}}
			</li>
			</ul>
			<br/>
			<ul class="iconlist border_top_bottom">
				<li class="download">
				<?php 
					if(!empty($val->map)){
						$map_url = "/upload/images/contactus/".$val->map;
						?><a target="_blank" href="{{$map_url}}">MAP DOWNLOAD</a><?php
					}else{
						echo "MAP DOWNLOAD";
					}
				?>
				</li>
				<li class="map"><a target="_blank" href="https://www.google.co.th/maps/dir//14.521019,100.9289237/@14.5217567,100.8959001,13z/data=!3m1!4b1!4m3!4m2!1m0!1m0?hl=en">GOOGLE MAP</a></li>
			</ul>
			<br/>
		


	@include('frontend._layouts.social_box')

	</div>

	<!--Side Bar Start -->
	<div class="side_bar">
		@include('frontend._layouts.recent_projects_box')
		<br/>
		<br/>
		@include('frontend._layouts.link_box')
	</div>
	<!-- Side Bar End -->

	</div>

</div>

@stop