@extends('frontend._layouts.master')

@section('content')
@include('frontend._layouts.header')  
@include('frontend._layouts.menu')
@include('frontend._layouts.main_slider')

<div class="content_box">

	<div class="main_content content_width row">
		
	<div class="content">
		<!-- What we do Start -->
			<div class="what_we_do">
				@foreach($data['whatWeDo'] as $what)
					<img src="/upload/images/what-we-do/{{$what->image_thumb}}"/>
					<div class="main_topic">@if(Session::get('lang') == "en"){{$what->title_en}} @else {{$what->title}} @endif</div>
					<p>@if(Session::get('lang') == "en"){{$what->description_en}}  @else {{$what->description}} @endif
					<a href="portfolio" class="view_detail">View <span class="glyphicon glyphicon-chevron-right glyphiconThin"></span></a>
					</p>
				@endforeach
			</div>
		<!-- What we do End -->

		<!-- Recent Project Start -->
			<div class="recent_project_box">
				<div class="main_topic">RECENT PROJECTS</div>
				
						<div id="recent_project_slide" class="recent_project_slide">
								@foreach($data['recent_project'] as $pro)
									<div class="project_thumb">
										<a class="fancybox_overlay fancybox.ajax" data-fancybox-type="ajax" href="{{URL::to('/')}}/{{Request::segment(1)}}/project_detail/{{$pro->id}}">
											<img src="/upload/images/project/{{$pro->image}}"/>
											@if(Session::get('lang') == "en")
												<div class="item_topic_recent_project">{{$pro->title_en}}</div>
												<div class="project_desc">{{Str::limit($pro->description_en, 90, '..')}}</div>
											@else
												<div class="item_topic_recent_project">{{$pro->title}}</div>
												<div class="project_desc">{{Str::limit($pro->description, 90, '..')}}</div>
											@endif
										</a>
									</div>
								@endforeach

							</div>	

						<a class="btn-view-all displayBlock pull-right marginRight5" href="portfolio">
		          View All 
		      	</a>


				<p>&nbsp;</p><p>&nbsp;</p>


			</div>
		<!-- Recent Project End -->

	@include('frontend._layouts.social_box')

	</div>

	<!--Side Bar Start -->
	<div class="side_bar">
		@include('frontend._layouts.activities_box')
		<br/>
		<br/>
		@include('frontend._layouts.link_box')
	</div>
	<!-- Side Bar End -->

	</div>

</div>

@stop