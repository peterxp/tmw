<?php

return array(
    'home'              =>'HOME',
    'services'          =>'SERVICES',
    'portfolio'          =>'PORTFOLIO',
    'activities'        =>'ACTIVITIES',
    'our_customer'      =>'OUR CUSTOMERS',
    'aboutus'           =>'ABOUT US',
    'job_vacancy'       =>'JOB VACANY',
    'contactus'         =>'CONTACT US',
    'recent_projects'	=>'RECENT PROJECTS'
);