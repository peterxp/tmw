<?php

return array(
    'home'              =>'หน้าหลัก',
    'services'          =>'บริการ',
    'portfolio'          =>'พอร์ตโฟลิโอ',
    'activities'        =>'กิจกรรม',
    'our_customer'      =>'ลูกค้าของเรา',
    'aboutus'           =>'เกี่ยวกับเรา',
    'job_vacancy'       =>'รับสมัครงาน',
    'contactus'         =>'ติดต่อเรา',
    'recent_projects'	=>'โครงการล่าสุด'
);