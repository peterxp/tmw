$(document).ready(function(){


	// Main Menu Start
	(function($){
		
		//cache nav
		var nav = $("#topNav");
		
		//add indicator and hovers to submenu parents
		nav.find("li").each(function() {
			if ($(this).find("ul").length > 0) {
				// $("<span>").text(">").appendTo($(this).children(":first"));
				$("<span class='glyphicon glyphicon-chevron-down glyphiconThin marginTop4'>").appendTo($(this).children(":first"));

				//show subnav on hover
				$(this).mouseenter(function() {
					$(this).find("ul").stop(true, true).slideDown();
				});
				
				//hide submenus on exit
				$(this).mouseleave(function() {
					$(this).find("ul").stop(true, true).slideUp();
				});
			}
		});
	})(jQuery);
	// Main Menu End


	$('.carousel').carousel({
		interval: 5000,
    	wrap: true
	});

  	$('#recent_project_slide').bxSlider({
	    slideWidth: 655,
	    minSlides: 2,
	    maxSlides: 2,
	    slideMargin: 5,
	    autoStart: true,
	    pager: true,
	    infiniteLoop: false,
	    hideControlOnEnd: true
  	});


	$('.myAccordion').on('shown.bs.collapse', function () {
	   	$(this).children().first().css('background-color', '#c9c9c9');
	});

	$('.myAccordion').on('hidden.bs.collapse', function () {
	   	$(this).children().first().css('background-color', '#e6e6e6');
	});


});