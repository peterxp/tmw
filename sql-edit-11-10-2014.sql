-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 11, 2014 at 01:31 AM
-- Server version: 5.5.38-35.2
-- PHP Version: 5.4.23

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `peterxp_tmw`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE IF NOT EXISTS `aboutus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `title_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`id`, `title`, `title_en`, `description`, `description_en`, `image`, `image1`, `image2`, `image3`, `image4`, `created_at`, `updated_at`) VALUES
(1, 'THONGMIT WATTANAKITRUNGRUANG CO.,LTD', 'THONGMIT WATTANAKITRUNGRUANG CO.,LTD', 'TMW is saraburi base construction company was founded in 2003 with registered capital of 5,000,000 Baht.Through many economic ups and downs, TMW has continued to prosper. Our honesty, integrity, and upfront approach have allowed us to become who we are today and will guide us into the future.', 'TMW is saraburi base construction company was founded in 2003 with registered capital of 5,000,000 Baht.Through many economic ups and downs, TMW has continued to prosper. Our honesty, integrity, and upfront approach have allowed us to become who we are today and will guide us into the future.\r\n', 'BRQVw0jh_13092014-055854.jpg', 'B971wAQP_30082014-093447.jpg', 'bfg6Rl2Z_30082014-093447.jpg', 'IpSLJYuJ_30082014-093447.jpg', 'MmgStHCn_30082014-093447.jpg', '2014-03-18 17:00:00', '2014-10-09 05:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `title`, `title_en`, `description`, `description_en`, `image`, `image_thumb`, `created_at`, `updated_at`) VALUES
(15, 'ปลูกป่าเฉลิมพรเกียรติ', 'Growing Plant', 'กิจกรรมปลูกป่าเฉลิมพระเกียรติ เนื่องในวัน 12 สิงหาคม 2557', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '11092014-085150.jpg', 'thumb_11092014-085150.jpg', '2014-09-10 19:47:29', '2014-09-11 01:51:55'),
(16, 'ปลูกปะการังวันสิ่งแวดล้อมโลก', 'Coral Plant', 'ปลูกปะการังวันสิ่งแวดล้อมโลก', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '11092014-085650.jpg', 'thumb_11092014-085650.jpg', '2014-09-10 22:35:07', '2014-09-11 02:00:01'),
(17, 'ทำบุญ 9 วัด', '9 Wats', 'กิจกรรมทำบุญ 9 วันเพื่อความเป็นศิริมงคล', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '11092014-092152.jpg', 'thumb_11092014-092152.jpg', '2014-09-10 22:39:23', '2014-09-11 02:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `activitygalleries`
--

CREATE TABLE IF NOT EXISTS `activitygalleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activitygalleries_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `activitygalleries`
--

INSERT INTO `activitygalleries` (`id`, `activitygalleries_id`, `image`, `image_thumb`, `created_at`, `updated_at`) VALUES
(1, 15, '12092014-152053.jpg', 'thumb_12092014-152053.jpg', '2014-09-10 19:48:47', '2014-09-12 08:21:00'),
(2, 15, '11092014-075754.jpg', 'thumb_11092014-075754.jpg', '2014-09-10 19:49:01', '2014-09-11 00:58:01'),
(3, 15, '11092014-075807.jpg', 'thumb_11092014-075807.jpg', '2014-09-10 19:49:15', '2014-09-11 00:58:13'),
(4, 15, '11092014-075821.jpg', 'thumb_11092014-075821.jpg', '2014-09-10 23:33:23', '2014-09-11 00:58:28'),
(5, 15, '11092014-075835.jpg', 'thumb_11092014-075835.jpg', '2014-09-11 00:13:00', '2014-09-11 00:58:41'),
(11, 15, '11092014-082228.jpg', 'thumb_11092014-082228.jpg', '2014-09-11 01:18:09', '2014-09-11 01:22:34'),
(13, 16, '11092014-090040.jpg', 'thumb_11092014-090040.jpg', '2014-09-11 02:00:47', '2014-09-11 02:00:47'),
(15, 16, '11092014-090102.jpg', 'thumb_11092014-090102.jpg', '2014-09-11 02:01:11', '2014-09-11 02:01:11'),
(17, 16, '11092014-091115.jpg', 'thumb_11092014-091115.jpg', '2014-09-11 02:01:35', '2014-09-11 02:11:23'),
(18, 16, '11092014-091349.JPG', 'thumb_11092014-091349.JPG', '2014-09-11 02:13:57', '2014-09-11 02:13:57'),
(19, 16, '11092014-091402.jpg', 'thumb_11092014-091402.jpg', '2014-09-11 02:14:07', '2014-09-11 02:14:07'),
(21, 16, '11092014-091424.jpg', 'thumb_11092014-091424.jpg', '2014-09-11 02:14:35', '2014-09-11 02:14:35'),
(22, 17, '14092014-003412.JPG', 'thumb_14092014-003412.JPG', '2014-09-11 02:22:20', '2014-09-14 05:34:36'),
(23, 17, '11092014-092610.jpg', 'thumb_11092014-092610.jpg', '2014-09-11 02:22:33', '2014-09-11 02:26:18'),
(25, 17, '11092014-092711.jpg', 'thumb_11092014-092711.jpg', '2014-09-11 02:22:54', '2014-09-11 02:27:21'),
(26, 17, '11092014-092259.jpg', 'thumb_11092014-092259.jpg', '2014-09-11 02:23:06', '2014-09-11 02:23:06'),
(27, 17, '11092014-092311.jpg', 'thumb_11092014-092311.jpg', '2014-09-11 02:23:19', '2014-09-11 02:23:19'),
(28, 17, '11092014-092325.JPG', 'thumb_11092014-092325.JPG', '2014-09-11 02:23:38', '2014-09-11 02:23:38');

-- --------------------------------------------------------

--
-- Table structure for table `contactuses`
--

CREATE TABLE IF NOT EXISTS `contactuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `address_en` text COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contactuses`
--

INSERT INTO `contactuses` (`id`, `company_name`, `company_name_en`, `address`, `address_en`, `province`, `province_en`, `post_code`, `email`, `map`, `facebook_url`, `twitter_url`, `instagram_url`, `youtube_url`, `telephone`, `mobile`, `fax`, `created_at`, `updated_at`) VALUES
(1, 'ทีเอ็ม ดับบลิว', 'TMW CO., LTD.', '33/3 หมู่ 5 ตำบลหนองปลาไหล อำเภอเมือ	ง', '33/3 Moo 5, Tambon Nongplalai, Amphure Muang', 'จังหวัดสระบุรี', 'Saraburi', '18000', 'info@tmw-construction.com', 'map.jpg', 'https://www.facebook.com', 'https://www.twitter.com', 'https://instagram.com/', 'http://www.youtube.com/results?search_query=construction+engineering+technology', '036-213127', '087-722-2841', '02-2345433', '2014-04-01 00:26:26', '2014-08-28 03:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `customer_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `title`, `customer_logo`, `created_at`, `updated_at`) VALUES
(1, 'Construction Marketing', '1_11092014-184812.jpg', '2014-08-29 21:05:55', '2014-09-11 11:48:12'),
(2, 'Maple Brook Construction', '2_11092014-185534.jpg', '2014-08-29 21:06:30', '2014-09-11 11:55:34'),
(3, 'Eden Construction', '3_11092014-185552.jpg', '2014-08-29 21:14:48', '2014-09-11 11:55:52'),
(4, 'Construction Company', '4_11092014-185619.jpg', '2014-08-29 21:48:37', '2014-09-11 11:56:19'),
(5, 'Wendy''s Construction', '5_11092014-185640.jpg', '2014-08-29 22:13:56', '2014-09-11 11:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '{"admin":1,"users":1}', '2014-03-07 09:05:39', '2014-03-07 09:05:39');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic` text COLLATE utf8_unicode_ci NOT NULL,
  `topic_en` text COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `detail_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `topic`, `topic_en`, `detail`, `detail_en`, `created_at`, `updated_at`) VALUES
(1, 'วิศกรฝ่ายขาย [TH]', 'Project Sales Engineer [EN]', '<p>Job Descriptions: [TH] To perform technical sales function exploit the growth potential of stainless steel product in SEA, actively marketing the stainless steel products to end users, distributors, designers, specifiers, engineers and other relevant approval bodies in co-operation with the Group Marketing Strategic so as to achieve widespread incorporation into suitable projects. Qualifications: Educational background in Mechanical / Civil Engineering. Experienced in Technical Sales, Product Engineer, Project Engineering from any engineering or construction industries. Experienced from steel or related business is a plus (but not a must) as the training will be provided. Familiar to work with EPC, Engineering team. Good commercial mindset, presentation skill, flexible, result-oriented and ability to travel to oversea and upcountry occasionally. Good command of English communication skills. ﻿</p>\r\n', '<p>Job Descriptions: [EN] To perform technical sales function exploit the growth potential of stainless steel product in SEA, actively marketing the stainless steel products to end users, distributors, designers, specifiers, engineers and other relevant approval bodies in co-operation with the Group Marketing Strategic so as to achieve widespread incorporation into suitable projects. Qualifications: Educational background in Mechanical / Civil Engineering. Experienced in Technical Sales, Product Engineer, Project Engineering from any engineering or construction industries. Experienced from steel or related business is a plus (but not a must) as the training will be provided. Familiar to work with EPC, Engineering team. Good commercial mindset, presentation skill, flexible, result-oriented and ability to travel to oversea and upcountry occasionally. Good command of English communication skills. ﻿</p>\r\n', '2014-08-28 23:03:40', '2014-09-11 11:27:53'),
(2, 'วิศกรแผนก R&D(Construction Material) (20K-50K) [TH]', 'R&D Engineer(Construction Material) (20K-50K) [EN]', '<p>Qualifications: [TH] Bachelor or Master of Chemistry/Engineering or other related field Curious, analytical, high attention to detail problem solving skill Negotiation skill English Knowledge of EHS regulations 2-3 years of working experience as R&amp;D and experience in manufacturing environment Working experience in analytical method development and validation Experience in managing a team of staff and resource Responsibilities: To develop new product according to the company&rsquo;s strategy; To develop / initiate standards and methods for testing, inspection, and evaluation knowledge in engineering fields such as chemistry; To manage projects by allocating tasks to Lab Technicians and following up to get projects done in scheduled time; To communicate about the products to the marketing, sales teams and Weber worldwide; To control the performance of the products through trials; To get abreast with the innovations of the competitors ; To continuously look for new raw materials, new formula to minimize costs; To work with the marketing team to be competitive in terms of innovations in the market; To control and communicate R&amp;D procedures to concerned departments; To monitor and develop Lab Technicians&rsquo; skills. Qualified candidate please send your detailed resume stating your current package and expectation to</p>\r\n', '<p>Qualifications: [EN] Bachelor or Master of Chemistry/Engineering or other related field Curious, analytical, high attention to detail problem solving skill Negotiation skill English Knowledge of EHS regulations 2-3 years of working experience as R&amp;D and experience in manufacturing environment Working experience in analytical method development and validation Experience in managing a team of staff and resource Responsibilities: To develop new product according to the company&rsquo;s strategy; To develop / initiate standards and methods for testing, inspection, and evaluation knowledge in engineering fields such as chemistry; To manage projects by allocating tasks to Lab Technicians and following up to get projects done in scheduled time; To communicate about the products to the marketing, sales teams and Weber worldwide; To control the performance of the products through trials; To get abreast with the innovations of the competitors ; To continuously look for new raw materials, new formula to minimize costs; To work with the marketing team to be competitive in terms of innovations in the market; To control and communicate R&amp;D procedures to concerned departments; To monitor and develop Lab Technicians&rsquo; skills. Qualified candidate please send your detailed resume stating your current package and expectation to</p>\r\n', '2014-08-28 23:04:44', '2014-09-11 11:27:21'),
(3, 'ผู้จัดการฝ่ายสถาปัตย์ [TH]', 'Architecture Manager', '<p><strong>Job Descriptions: &nbsp;[TH]</strong></p>\r\n\r\n<p>To perform technical sales function exploit the growth potential of stainless steel product in SEA, actively marketing the stainless steel products to end users, distributors, designers, specifiers, engineers and other relevant approval bodies in co-operation with the Group Marketing Strategic so as to achieve widespread incorporation into suitable projects.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Qualifications:</strong></p>\r\n\r\n<ul>\r\n	<li>Educational background in Mechanical / Civil Engineering.</li>\r\n	<li>Experienced in Technical Sales, Product Engineer, Project Engineering from any engineering or construction&nbsp;industries. Experienced from steel or related business is a plus (but not a must) as the training will be provided.</li>\r\n	<li>Familiar to work with EPC, Engineering team.</li>\r\n	<li>Good commercial mindset, presentation skill, flexible, result-oriented&nbsp;and ability to travel to oversea and upcountry occasionally.</li>\r\n	<li>Good command of English communication skills. ﻿</li>\r\n</ul>\r\n', '<p><strong>Job Descriptions [EN]:</strong></p>\r\n\r\n<p>To perform technical sales function exploit the growth potential of stainless steel product in SEA, actively marketing the stainless steel products to end users, distributors, designers, specifiers, engineers and other relevant approval bodies in co-operation with the Group Marketing Strategic so as to achieve widespread incorporation into suitable projects.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Qualifications:</strong></p>\r\n\r\n<ul>\r\n	<li>Educational background in Mechanical / Civil Engineering.</li>\r\n	<li>Experienced in Technical Sales, Product Engineer, Project Engineering from any engineering or construction&nbsp;industries. Experienced from steel or related business is a plus (but not a must) as the training will be provided.</li>\r\n	<li>Familiar to work with EPC, Engineering team.</li>\r\n	<li>Good commercial mindset, presentation skill, flexible, result-oriented&nbsp;and ability to travel to oversea and upcountry occasionally.</li>\r\n	<li>Good command of English communication skills. ﻿</li>\r\n</ul>\r\n', '2014-08-31 03:40:10', '2014-09-14 05:21:52');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `title`, `link_url`, `link_banner`, `created_at`, `updated_at`) VALUES
(1, 'JOB Saraburi', 'http://google.com', '1_28082014-165219.jpg', '2014-08-28 09:52:19', '2014-08-28 17:14:03'),
(2, 'Technician', 'http://google.com', '2_28082014-165250.jpg', '2014-08-28 09:52:50', '2014-08-28 17:13:58'),
(3, 'Architect Fair', 'http://google.com', '3_28082014-165343.jpg', '2014-08-28 09:53:43', '2014-08-28 17:13:54'),
(4, 'Home Construction', 'http://google.com', '4_28082014-165403.jpg', '2014-08-28 09:54:03', '2014-08-28 17:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_03_27_032337_create_person_table', 1),
('2014_03_29_162536_create_products_table', 2),
('2014_03_31_170117_create_productGalleries_table', 3),
('2014_04_03_164230_create_contactuses_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `projectcategories`
--

CREATE TABLE IF NOT EXISTS `projectcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(4) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `projectcategories`
--

INSERT INTO `projectcategories` (`id`, `parent_id`, `title`, `title_en`, `description`, `description_en`, `image`, `created_at`, `updated_at`) VALUES
(1, 0, 'พลังงาน', 'POWER / ENERGY', 'TMW has experienced in construction of various power & energy projects for industrial clients i.e., Low pressure boiler(LP),Gas Turbine Generator(GTG),Gas Engine(GE) and Bio Gas. ', 'TMW has experienced in construction of various power & energy projects for industrial clients i.e., Low pressure boiler(LP),Gas Turbine Generator(GTG),Gas Engine(GE) and Bio Gas. ', '1_29082014-043918.png', '2014-08-28 21:39:18', '2014-10-03 15:20:18'),
(2, 0, 'โรงงาน', 'INDUSTRIAL', 'TMW has involved and experienced in construction services to various insdustial projects that require expertize skills and high safety standard', 'TMW has involved and experienced in construction services to various insdustial projects that require expertize skills and high safety standard', '2_29082014-044018.png', '2014-08-28 21:40:18', '2014-10-09 11:47:38'),
(3, 0, 'งานโครงสร้าง', 'INFRASTRUCTURE', 'TMW has invloved and experienced in construction of various infrastruture projects i.e., drainage,road and concrete work.', 'TMW has invloved and experienced in construction of various infrastruture projects i.e., drainage,road and concrete work.', '3_29082014-044037.png', '2014-08-28 21:40:37', '2014-10-09 11:54:17'),
(4, 0, 'วิศกรรมเครื่องกล', 'MACHANICAL ENGINEERING', 'TMW is equipped and capable of handling any of your mechanical engineering needs in wide ranges of various sizes and scales.  ', 'TMW is equipped and capable of handling any of your mechanical engineering needs in wide ranges of various sizes and scales.  ', '4_29082014-044058.png', '2014-08-28 21:40:58', '2014-10-09 12:37:40'),
(5, 0, 'สิ่งปลูกสร้าง', 'BUILDING', 'TMW provides building construction in wide ranges of various sizes and scales i.e., resident building, commercial building,Store-Factory Building,Office building car park, warehouse.', 'TMW provides building construction in wide ranges of various sizes and scales i.e., resident building, commercial building,Store-Factory Building,Office building car park, warehouse.', '5_29082014-044121.png', '2014-08-28 21:41:21', '2014-10-09 11:44:28'),
(6, 0, 'สิ่งแวดล้อม', 'ENVIRONMENTAL', 'TMW has involved and experienced in construction of various environmental projects i.e., Waste Water Treatment and Water supply system, and is ready for the expansion in environmental business according to development of the country.', 'TMW has involved and experienced in construction of various environmental projects i.e., Waste Water Treatment and Water supply system, and is ready for the expansion in environmental business according to development of the country.', '6_29082014-044138.png', '2014-08-28 21:41:38', '2014-10-09 11:47:03'),
(8, 5, 'Commercial Building', 'Commercial Building', 'Building 1', 'Building 1', '8_30092014-150513.png', '2014-09-30 07:09:23', '2014-10-03 10:07:22'),
(9, 5, 'Store-Factory Building', 'Store-Factory Building', 'Building 2', 'Building 2', '9_30092014-150522.png', '2014-09-30 07:11:08', '2014-10-03 10:08:44'),
(13, 6, 'West Water Treatment Plant', 'West Water Treatment Plant', 'En 1', 'En 1', '13_30092014-145134.png', '2014-09-30 07:51:34', '2014-10-03 10:11:18'),
(14, 5, 'งานก่อสร้างบ้านพักอาศัย', 'Residental Building', 'Bu3', 'Bu3', '', '2014-10-02 05:29:00', '2014-10-03 10:05:30'),
(15, 2, 'Industrial Plant', 'Industrial Plant', '', '', '', '2014-10-03 10:15:30', '2014-10-03 10:15:30'),
(16, 2, 'Renovation Work', 'Renovation Work', '', '', '', '2014-10-03 10:16:58', '2014-10-03 10:16:58'),
(17, 3, 'Road', 'Road', '', '', '', '2014-10-03 10:17:50', '2014-10-03 10:17:50'),
(18, 3, 'Drainage', 'Drainage', '', '', '', '2014-10-03 10:18:27', '2014-10-03 10:18:27'),
(19, 4, 'Stainless Steel Work', 'Stainless Steel Work', '', '', '', '2014-10-03 10:19:52', '2014-10-03 10:19:52'),
(20, 4, 'Fablication Work', 'Fablication Work', '', '', '', '2014-10-03 10:21:06', '2014-10-03 10:21:06'),
(21, 4, 'Installation Work', 'Installation Work', '', '', '', '2014-10-03 10:24:22', '2014-10-03 10:24:22'),
(22, 4, 'Steel Structure Work', 'Steel Structure Work', '', '', '', '2014-10-03 10:25:20', '2014-10-03 10:25:20'),
(23, 4, 'Machine Work', 'Machine Work', '', '', '', '2014-10-03 10:26:03', '2014-10-03 10:26:03'),
(24, 4, 'Maintainance Work', 'Maintainance Work', '', '', '', '2014-10-03 10:26:49', '2014-10-03 10:26:49');

-- --------------------------------------------------------

--
-- Table structure for table `projectgalleries`
--

CREATE TABLE IF NOT EXISTS `projectgalleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `projectgalleries`
--

INSERT INTO `projectgalleries` (`id`, `project_id`, `image`, `image_thumb`, `created_at`, `updated_at`) VALUES
(15, 12, '30082014-015122.jpg', 'thumb_30082014-015122.jpg', '2014-08-29 18:28:45', '2014-08-29 18:51:34'),
(16, 12, '30082014-013454.jpg', 'thumb_30082014-013454.jpg', '2014-08-29 18:35:00', '2014-08-29 18:35:01'),
(17, 12, '30082014-015257.jpeg', 'thumb_30082014-015257.jpeg', '2014-08-29 18:53:04', '2014-08-29 18:53:04'),
(18, 12, '30082014-015326.jpg', 'thumb_30082014-015326.jpg', '2014-08-29 18:54:13', '2014-08-29 18:54:13'),
(19, 12, '30082014-015856.JPG', 'thumb_30082014-015856.JPG', '2014-08-29 18:59:03', '2014-08-29 18:59:03'),
(20, 16, '11092014-202022.jpg', 'thumb_11092014-202022.jpg', '2014-09-11 13:16:50', '2014-09-11 13:20:30'),
(21, 16, '11092014-202038.jpg', 'thumb_11092014-202038.jpg', '2014-09-11 13:20:45', '2014-09-11 13:20:45'),
(22, 14, '12092014-153608.JPG', 'thumb_12092014-153608.JPG', '2014-09-11 13:36:15', '2014-09-12 08:36:16'),
(23, 14, '12092014-144609.jpg', 'thumb_12092014-144609.jpg', '2014-09-11 13:36:39', '2014-09-12 07:46:19'),
(24, 14, '11092014-203643.jpg', 'thumb_11092014-203643.jpg', '2014-09-11 13:36:53', '2014-09-11 13:36:54'),
(25, 14, '12092014-144543.jpg', 'thumb_12092014-144543.jpg', '2014-09-11 13:37:13', '2014-09-12 07:45:56'),
(26, 14, '12092014-144629.JPG', 'thumb_12092014-144629.JPG', '2014-09-11 13:37:28', '2014-09-12 07:46:38'),
(27, 14, '12092014-153759.jpg', 'thumb_12092014-153759.jpg', '2014-09-12 07:55:45', '2014-09-12 08:38:09'),
(28, 13, '12092014-151316.jpg', 'thumb_12092014-151316.jpg', '2014-09-12 08:13:24', '2014-09-12 08:13:24');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projectcategories_id` int(7) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `projectcategories_id`, `title`, `title_en`, `description`, `description_en`, `image`, `image_thumb`, `created_at`, `updated_at`) VALUES
(12, 1, 'โครงการพลังงานแสงอาทิตย์ 1', 'Solar Cell Project 1', 'Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์  งบประมาณ 15 ล้านบาท ระยะเวลาในการดำเนินการ 24 มิ.ย. 2557- 31 ธ.ค. 2557', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. \r\nBudget: 12 Million Baht\r\nPeriod 24 Jun 2014 - 31 Oct 2014\r\n', '31082014-160021.jpg', 'thumb_31082014-160021.jpg', '2014-08-29 17:53:37', '2014-10-02 09:31:56'),
(13, 4, 'โครงการบ่อบำบัดน้ำเสีย', 'Lorem Ipsum is simply dummy text ', 'เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์ มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16 ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, ', '31082014-160415.jpg', 'thumb_31082014-160415.jpg', '2014-08-31 07:46:18', '2014-09-11 09:12:05'),
(14, 9, 'โครงการบ้านแสนสุข', 'Happy Home', 'ตรงกันข้ามกับความเชื่อที่นิยมกัน Lorem Ipsum ไม่ได้เป็นเพียงแค่ชุดตัวอักษรที่สุ่มขึ้นมามั่วๆ แต่หากมีที่มาจากวรรณกรรมละตินคลาสสิกชิ้นหนึ่งในยุค 45 ปีก่อนคริสตศักราช ทำให้มันมีอายุถึงกว่า 2000 ปีเลยทีเดียว ', 'Project:Happy Home\r\nScope of Work:Single home 4 rooms with 2 bathroom.\r\nProject Owner:Khun Sompong\r\nContact Value:5,000,000 baht', '12092014-145148.JPG', 'thumb_12092014-145148.JPG', '2014-08-31 07:47:43', '2014-10-04 05:35:11'),
(15, 9, 'Test Sub ', 'Test Sub EN', 'Test Sub', 'Test Sub EN', '02102014-000709.jpg', 'thumb_02102014-000709.jpg', '2014-09-30 22:36:06', '2014-10-02 05:07:36');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `title_en`, `description`, `description_en`, `image`, `image_thumb`, `created_at`, `updated_at`) VALUES
(17, 'ก่อสร้างอาคาร ', 'Construction Building', 'Description of Construction Building [TH]', 'Description of Construction Building [EN]', '13092014-053427.jpg', 'thumb_13092014-053427.jpg', '2014-08-30 21:29:59', '2014-09-12 22:34:53'),
(18, 'บ่อบัดบัดน้ำเสีย', 'Waste Water Threament', 'Description of Waste Water Threament', 'Description of Waste Water Threament', '13092014-053219.jpg', 'thumb_13092014-053219.jpg', '2014-08-30 21:57:26', '2014-09-12 22:32:34'),
(22, 'ก่อสร้างโรงงาน', 'Factory Construction', 'งานก่อสร้างอาคารโรงงาน', '[EN] It is a long established fact that a reader will be distracted by the readable content of a page.', '13092014-053627.jpg', 'thumb_13092014-053627.jpg', '2014-09-11 07:40:51', '2014-09-25 14:48:02');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE IF NOT EXISTS `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(2, 1, '127.0.0.1', 0, 0, 0, NULL, NULL, NULL),
(3, 2, '127.0.0.1', 0, 0, 0, NULL, NULL, NULL),
(4, 2, '49.0.113.110', 0, 0, 0, NULL, NULL, NULL),
(5, 2, '125.26.216.129', 0, 0, 0, NULL, NULL, NULL),
(6, 2, '125.26.215.125', 0, 0, 0, NULL, NULL, NULL),
(7, 2, '125.26.214.126', 0, 0, 0, NULL, NULL, NULL),
(8, 1, '49.0.113.194', 0, 0, 0, NULL, NULL, NULL),
(9, 1, '49.0.97.121', 0, 0, 0, NULL, NULL, NULL),
(10, 1, '49.0.97.86', 0, 0, 0, NULL, NULL, NULL),
(11, 2, '125.26.252.204', 0, 0, 0, NULL, NULL, NULL),
(12, 2, '125.26.238.183', 0, 0, 0, NULL, NULL, NULL),
(13, 2, '125.26.254.210', 0, 0, 0, NULL, NULL, NULL),
(14, 2, '125.26.223.121', 0, 0, 0, NULL, NULL, NULL),
(15, 2, '125.26.243.150', 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'peterxp@hotmail.com', '$2y$10$.5w3NsQcmM5BbLOkNARhpeY2VvsvN7ECvyk7KW9/2BSo/FvUeodOa', NULL, 1, NULL, NULL, '2014-10-02 09:12:21', '$2y$10$NRDPTbU603nrcBINy6ljC.5oUkrhKWzQAUiV3tGVtaGFbmkqR92VO', NULL, 'PeterXP', 'Jing Jing Na', '2014-03-07 09:20:42', '2014-10-02 09:12:21'),
(2, 'chainsee@gmail.com', '$2y$10$7CbMS/HhHuc.NzXBYNukNemPjZjsM/UlHKzYMFzRsD5POIiXNqAIe', NULL, 1, NULL, NULL, '2014-10-11 08:13:35', '$2y$10$.trQkK7yFRr9GiiYxYOM2.EPaxxMc.BrPOMJbzCBUO.EmA8WsTX0K', NULL, 'Chainarong', 'Seeha', '2014-09-12 23:48:50', '2014-10-11 08:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `whats`
--

CREATE TABLE IF NOT EXISTS `whats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `whats`
--

INSERT INTO `whats` (`id`, `title`, `title_en`, `description`, `description_en`, `image`, `image_thumb`, `created_at`, `updated_at`) VALUES
(1, 'งานที่บริษัทเราทำ', 'What we do', 'บริษัท ทองมิตรวัฒนกิจรุ่งเรือง จำกัด ดำเนินธุรกิจรับเหมาก่อสร้างอาคารโรงงาน อาคารพานิชย์ บ้านพักอาศัยและงานปรับปรุง ต่อเติมต่างๆในโรงงานอุตสาหกรรม ก่อตั้งจากทีมงานคนรุ่นใหม่ที่มีประสบการณ์กว่า 10 ปีโดยลักษณะงานก่อสร้างจะมีตั้งแต่โครงการก่อสร้างที่เป็นโครงการขนาดเล็ก ขนาดกลาง ไปจนถึงโครงการขนาดใหญ่ มีผลงานคุณภาพระดับมาตรฐานสากลอันเป็นที่ยอมรับจากลูกค้า และยังคงมุ่งพัฒนาและก้าวต่อไปเพื่อสร้างชื่อเสียงและความเป็นผู้นำ เพื่อมุ่งสู่การเป็นบริษัทก่อสร้างชั้นนำของประเทศไทย', 'Thongmit Wattakitrungruang Co., Ltd., is a Saraburi base commercial construction firm providing traditional general contracting, construction management and design-build services.  Our team has executed projects for government entities and private clients that span many categories of work and include new construction, additions, renovations and repairs.We execute small and large construction projects with safety and quality.Our site management and foremen are highly experienced and are able to deliver a wide range of architectural finishes to a high standard.while maintaining a culture of continuous improvement throughout the company.\r\nWe are committed to integrity and trust. Exceeding our client’s expectations is our passion, and our list of satisfied customers continues to grow. We emphasize building partnerships and long-term relationships with every client.  ', '13092014-054434.jpg', 'thumb_13092014-054434.jpg', '2014-08-30 18:51:35', '2014-10-04 05:02:15');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
